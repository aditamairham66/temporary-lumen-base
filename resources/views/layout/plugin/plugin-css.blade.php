<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('pearl-ui/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('pearl-ui/css/feather.css') }}">
<link rel="stylesheet" href="{{ asset('pearl-ui/css/vendor.bundle.base.css') }}">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/simple-line-icons/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/font-awesome/css/font-awesome.min.css') }}" />

<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
<link rel='stylesheet' href="{{ asset('pearl-ui/vendors/lightbox/dist/css/lightbox.min.css') }}"/>

<!-- <link rel="stylesheet" href="{{ asset('pearl-ui/vendors/icheck/skins/all.css') }}"> -->
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('flatpickr/flatpickr.min.css') }}">
<style>
    .flatpickr-calendar.open {
        display: inline-block;
        z-index: 1;
    }
</style>

<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('pearl-ui/css/style.css') }}">
<!-- endinject -->
<link rel="shortcut icon" href="{{ asset('pearl-ui/images/favicon.png') }}" />
<style>
    /* UPLOAD FILE CUSTOM CSS */
    /* HIDDEN INPUT */
    .inputfile {
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    /* END HIDDEN INPUT */
    /* LABEL */
    .inputfile + label img{
        height: 155px;
        width: 155px;
        object-fit: cover;
        /* opacity: .2; */
    }
    .inputfile + label {
        font-size: 1.25em;
        font-weight: 700;
        color: #959595;
        background-color: transparent;
        border-radius: 8px;
        display: inline-block;
        font-family: 'Bold';
        font-size: 12px;
        font-weight: 600;
        line-height: 14px;
        text-align: center;
        border: 2px solid #D6D8D9;
        padding:13px 15px;
        transition:.5s;		
    }
    .inputfile:focus + label,
    .inputfile + label:hover, .inputfile + label:hover  img{
        border-color: #333;
        transition:.5s;
        opacity:1;
    }
    /* END LABEL */
    /* CURSOR */
    .inputfile + label {
        cursor: pointer; /* "hand" cursor */
    }
    /* END CURSOR */
    .inputfile:focus + label {
        outline: 1px dotted #959595;
        border-color:none;
        border-radius: 8px;
    }
    .inputfile:focus + label img{
        opacity: 1;
    }
    /* END UPLOAD FILE CUSTOM CSS */
</style>