<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCmsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("name",255)->nullable();
            $table->string("email",255)->nullable();
            $table->string("image",255)->nullable();
            $table->text("password")->nullable();
            $table->integer("roles_id")->nullable();
            $table->integer("md_cabang_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_users');
    }
}
