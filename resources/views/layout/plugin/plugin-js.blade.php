<!-- plugins:js -->
<script src="{{ asset('pearl-ui/js/vendor.bundle.base.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('pearl-ui/js/off-canvas.js') }}"></script>
<script src="{{ asset('pearl-ui/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('pearl-ui/js/misc.js') }}"></script>
<script src="{{ asset('pearl-ui/js/settings.js') }}"></script>
<script src="{{ asset('pearl-ui/js/todolist.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<!-- End custom js for this page-->
<!-- plugin js for this page -->
<script src="{{ asset('pearl-ui/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('pearl-ui/js/alerts.js') }}"></script>
<script src="{{ asset('pearl-ui/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('pearl-ui/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('pearl-ui/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('flatpickr/flatpickr.js') }}"></script>
<script src="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('pearl-ui/vendors/lightbox/dist/js/lightbox.min.js') }}"></script>
<!-- End plugin js for this page -->

<script>
// document.onreadystatechange = function () {
//     var state = document.readyState
//     if (state == 'interactive') {
//         $(".container-scroller").hide();
//     } else if (state == 'complete') {
//         setTimeout(function(){
//             $(".container-scroller").show();
//             // $(".jumping-dots-loader").css("display","none");
//             // $(".jumping-dots-loader").hide();
//         },1000);
//     }
// }

(function($) {
'use strict';
	if ($(".js-example-basic-single").length) {
		$(".js-example-basic-single").select2();
	}
	if ($(".js-example-basic-multiple").length) {
		$(".js-example-basic-multiple").select2();
	}
	/*Summernote editor*/
	if ($("#summernoteExample").length) {
		$('#summernoteExample').summernote({
			height: 300,
			tabsize: 2
		});
	}
})(jQuery);

'use strict';
		;( function ( document, window, index )
{
	var inputs = document.querySelectorAll( '.inputfile' );
	Array.prototype.forEach.call( inputs, function( input )
	{
		var label	 = input.nextElementSibling,
			labelVal = label.innerHTML;

		input.addEventListener( 'change', function( e )
		{
            readURL(this);
			var fileName = '';
			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				label.querySelector( 'span' ).innerHTML = fileName;
			else
				label.innerHTML = labelVal;
		});

        function readURL(input) {
            console.log(input.files);
            if (input.files) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                    console.log( e.target.result);
                    console.log(input.id);
                    $('#show_'+input.id).attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

		// Firefox bug fix
		input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
		input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
	});
}( document, window, 0 ));
</script>