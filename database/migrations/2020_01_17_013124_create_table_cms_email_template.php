<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCmsEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_email_template', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string("name",255)->nullable();
            $table->string("slug",255)->nullable();
            $table->string("subject",255)->nullable();
            $table->text("content")->nullable();
            $table->string("description",255)->nullable();
            $table->string("from_name",255)->nullable();
            $table->string("from_email",255)->nullable();
            $table->string("cc_email",255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_email_template');
    }
}
