<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class MiddlewareAPIDefault
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $attributes;
    
    public function handle($request, Closure $next)
    {
        $useragent = API::isBrowserRequest($request->header("User-Agent"));
        if ($useragent) {
            return abort("404");
        } else {
            $get_token =  API::getTokenApi($request->header("authorization"));
            if ($get_token['ip_address'] == $request->ip() && $get_token['user_agent']==$request->header("User-Agent")) {
                
                $response['token'] = $get_token;
                $request->attributes->add($response);
                
            }else{
                $response['message'] = "Invalid parameters";
                return response()->json($response, 400);
            }
        }

        return $next($request);
    }
}
