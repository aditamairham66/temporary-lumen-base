<?php

use Illuminate\Database\Seeder;
use App\Helpers\API;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CreateSeedUsersInventaris extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table("users_invetaris")
            ->where("email","demo@bukopin.com")
            ->first();

        if ($check) {
            $this->command->info('Updating the data Users Invetaris failed, because data is same !');
        }else{
            $insert = DB::table("users_invetaris")
            ->insert([
                "created_at" => API::dateTime(),
                "id_privilage" => 1,
                "nik" => "demobukopin1",
                "md_cabang_id" => 1,
                "md_divisi_id" => 1,
                "md_lokasi_id" => 1,
                "unit" => "Demo",
                "nama" => "Demo Bukopin",
                "password" => Hash::make("123456"),
                "email" => "demo@bukopin.com",
                "keterangan" => "Akun demo satu bukopin",
            ]);
    
            if ($insert) {
                $this->command->info('Updating the data Users Invetaris completed !');
            }else{
                $this->command->info('Updating the data Users Invetaris failed !');
            }
        }
    }
}
