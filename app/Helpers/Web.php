<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Helpers\API;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Web
{
    // TODO Request valid
    public static function view($view, $erros, $request)
    {
        $data['request'] = $request;
        $data['errors'] = $erros;
        return view($view,$data);
    }

    // TODO Check valid
    public static function have($json,$data)
    {
        $decode = json_decode($json);
        if ($decode) {
            foreach ($decode as $key => $value) {
                if($key==$data){
                    return true;
                    break;
                }
            }
        }
    }

    // TODO get valid
    public static function get($json,$data)
    {
        $decode = json_decode($json);
        if ($decode) {
            foreach ($decode as $key => $value) {
                if($key==$data){
                    return $value[0];
                    break;
                }
            }
        }
    }

    // WEb old value
    public static function old($json, $data)
    {
        foreach ($json as $key => $value) {
            if($key==$data){
                return $value;
                break;
            }
        }
    }
    
    // TODO redirect
    public static function redirect($to, $msg, $type)
    {
        Cache::put($type,$msg,Carbon::now()->addSeconds(2));
        return redirect($to);
    }

    // TODO redirect page
    public static function page($type=null, $route, $parent_id = null, $status = null)
    {
        $link = "";
        if ($parent_id) {
            $link = "?parent_id=".$parent_id;
        }
        if(empty($type)){
            return redirect("/admin/".$route.$link);
        }else{
            if ($type=="Save") {
                return redirect("/admin/".$route.$link);
            }else{
                return redirect("/admin/".$route."/add".$link);
            }
        }
    }

    public static function hasError($params)
    {   
        $html = '';

        if (app('request')->session()->get('errors')) {
            if (Web::have(app('request')->session()->get('errors'),$params)) {
                $html = '<small class="text-danger">'.Web::get(app('request')->session()->get('errors'),$params).'</small>';
            }
        }

        return $html;
    }

    public static function withError($to, $params, $valid)
    {
        app('request')->session()->flash("errors",$valid);
        app('request')->session()->flash("request",$params);
        if (segmentPath(1)=="admin") {
            $to = "admin/".$to;
        }
        return redirect($to);
    }

    public static function msgPage($msg, $type, $params = null)
    {   
        $parent_id = app("request")->get("parent_id");
        $status = app("request")->get("status");
        
        app('request')->session()->flash("msg_type",$type);
        app('request')->session()->flash("msg_text",$msg);

        if (app("request")->segment(3)=="import-data") {
            $route = app("request")->segment(2)."/".app("request")->segment(3)."?done=1&file=".g("file")."&status=".g("status");
        }else{
            $route = app("request")->segment(2);
        }
        return Web::page(app('request')->input('submit'), $route,($parent_id?:null),($status?:null));
    }
    
    public static function Alert()
    {
        $type = app('request')->session()->get("msg_type");
        $msg = app('request')->session()->get("msg_text");
        
        if ($type=="warning") {
            $color = "alert-warning";
        }elseif ($type=="error") {
            $color = "alert-danger";
        }elseif ($type=="success") {
            $color = "alert-success";
        }elseif ($type=="bg-success") {
            $color = "bg-success";
        }elseif ($type=="bg-danger") {
            $color = "bg-danger";
        }elseif ($type=="bg-warning") {
            $color = "bg-warning";
        }
        
        $html = '';
        
        if ($type!="") {
            $html = '
            <div class="alert '.$color.'">
                <strong>Perhatian :</strong> '.$msg.'
            </div>
            ';
        }

        return $html;
    }

    public static function UploadFile($name, $path = null) {
        if (empty($path)) {
            $date = date('Y-m');
            $path = 'uploads/'.$date;
        }else{
            $date = date('Y-m');
            $path = 'uploads/'.$path;
        }

        if (app('request')->hasFile($name)){
            $file = app('request')->file($name);
            $filename = $file->getClientOriginalName();
            $ext = strtolower($file->getClientOriginalExtension());

            if($filename && $ext) {
                Storage::makeDirectory($path);
                $destinationPath = public_path($path);
                $extension        = $ext;
                $names            = rand(11111,99999).'.'.$extension;
                app('request')->file($name)->move($destinationPath, $names);
            }

            return $path.'/'.$names;
        }

    }

    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } elseif (file_exists($check_public)) {
            return url($path);
        } elseif (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    public static function imagePath($path = null)
    {
        if (Web::file($path)) {
            return Web::file($path);
        }else{
            return API::file("pearl-ui/images/favicon.png");
        }
    }

    // TODO SETTING GROUP
    public static function getSetting($key = null)
    {
        $data = DB::table("cms_setting")
            ->where("name", $key)
            ->first();

        return ($data->content=="" ? "" : $data->content);
    }

    public static function checkSetting($key = null)
    {
        $data = DB::table("cms_setting")
            ->where("name", $key)
            ->first();

        return $data;
    }

    public static function getSettingUpdate($key = null, $params = null)
    {
        $data = DB::table("cms_setting")
            ->where("name", $key)
            ->update([
                'content' => $params
            ]);

        return $data;
    }

    public static function getSettingInsert($key = null, $params = null)
    {
        $data = DB::table("cms_setting")
            ->insert([
                'name' => $key,
                'content' => $params
            ]);

        return $data;
    }

    public static function msgRedirectSetting($params, $msg, $type)
    {   
        app('request')->session()->flash("msg_type",$type);
        app('request')->session()->flash("msg_text",$msg);
        return Web::page($params->submit, "setting/show?group=".$params->get("group"));
    }

    // TODO dialogConfirm
    public static function dialogConfirm($url = null){
        if ($url == null || $url == '' || $url == 'javascript:void(0)') {
            $result = '';
        }else{
            $result = "swal({   
                title: 'Apakah anda yakin ?',   
                text: 'Tekan Ya untuk melanjutkan !',   
                type: 'warning',   
                showCancelButton: true,   
                confirmButtonColor: '#ff0000',   
                confirmButtonText: 'Yes!',  
                cancelButtonText: 'No',
                buttons: {
                    cancel: {
                        text: 'Tidak',
                        value: null,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true,
                    },
                    confirm: {
                        text: 'Ya',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true,
                    }
                }})
                .then(
                    function (confirm) {
                        if (confirm) {
                            location.href= '$url';
                        }
                    }
                )";
        }
        return $result;
    }
}

