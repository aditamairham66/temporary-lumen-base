<?php
if (!function_exists('segmentPath')) {
    /**
     * @param $segment
     *
     * @return string
     */
    function segmentPath($segment = null) {
        return app("request")->segment($segment);
    }
}

if (!function_exists('sessionGet')) {
    /**
     * @param $params
     *
     * @return string
     */
    function sessionGet($params = null) {
        return app("request")->session()->get($params);
    }
}

if (!function_exists('sessionPut')) {
    /**
     * @param string $params
     * @param string $value
     *
     * @return string
     */
    function sessionPut($params = null, $value = null) {
        return app("request")->session()->put($params, $value);
    }
}

if (!function_exists('oldW')) {
    /**
     * @param $params
     *
     * @return string
     */
    function oldW($params = null) {
        if (sessionGet("request")) {
            return App\Helpers\Web::old(sessionGet("request"),$params);
        }
    }
}

if (!function_exists('g')) {
    /**
     * @param $params
     *
     * @return string
     */
    function g($params = null) {
        return app('request')->get($params);
    }
}


// TODO GET IMAGE PATH
if (!function_exists('getLinkImage')) {
    /**
     * @param $key
     *
     * @return string
     */
    function getLinkImage($key = null)
    {
        return App\Helpers\Web::imagePath($key);
    }
}

// TODO GET IMAGE
if (!function_exists('getImage')) {
    /**
     * @param $key
     *
     * @return string
     */
    function getImage($key = null)
    {
        return App\Helpers\API::file($key);
    }
}

if (!function_exists('hasError')) {
    /**
     * Get Error from Form
     * @param $key
     * @return string
     */
    function hasError($key = null)
    {
        return App\Helpers\Web::hasError($key);
    }
}

if (!function_exists('showAlert')) {
    /**
     * Get Alert message method from form
     * @return HTML
     */
    function showAlert()
    {
        return App\Helpers\Web::Alert();
    }
}

if (!function_exists('dialogConfirm')) {
    /**
     * Get Alert message method to view
     * @param string $url
     * @return HTML
     */
    function dialogConfirm($url = null)
    {
        return App\Helpers\Web::dialogConfirm($url);
    }
}