-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table lumen_base.cms_email_template
CREATE TABLE IF NOT EXISTS `cms_email_template` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lumen_base.cms_email_template: ~1 rows (approximately)
/*!40000 ALTER TABLE `cms_email_template` DISABLE KEYS */;
INSERT IGNORE INTO `cms_email_template` (`id`, `created_at`, `updated_at`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`) VALUES
	(2, '2020-02-21 16:36:41', NULL, 'Forgot Password ', 'forgot_password', 'Forgot Password ', '<h4 style="font-family: &quot;Source Sans Pro&quot;, sans-serif; line-height: 1.1; color: rgb(51, 51, 51); margin-top: 10px; margin-bottom: 10px; font-size: 18px;"><font face="Arial"><span style="font-weight: 700;">Halo!</span></font></h4><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px;"><font color="#333333" face="Arial">Anda menerima email ini karena kami menerima permintaan kata sandi untuk akun Anda</font><font face="Arial" style="color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;">.</font></p><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px;"><font color="#333333" face="Arial">Silakan masuk dengan kata sandi baru Anda di aplikasi</font><font face="Arial" style="color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;">.&nbsp;</font></p><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;"><font face="Arial"><br></font></p><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: center;"><font face="Arial"></font><span style="background-color: rgb(60, 141, 188); cursor: default; color: white; padding: 15px 32px; font-size: 16px; display: inline-block; margin: 4px 2px;"><font face="Arial">[password]</font></span></p><p style="margin-right: 0px; margin-bottom: 10px; margin-left: 0px;"><br><font face="Arial">Salam,</font></p><p><font face="Arial" style="color: rgb(51, 51, 51);"></font></p><p style="box-sizing: border-box; margin: 0px 0px 10px;">Bukopin - Monitoring Aset Support&nbsp;&nbsp;</p>', 'Forgot Password ', 'Bukopin - Monitoring Aset Support ', 'info@crocodic.net', '');
/*!40000 ALTER TABLE `cms_email_template` ENABLE KEYS */;

-- Dumping structure for table lumen_base.cms_setting
CREATE TABLE IF NOT EXISTS `cms_setting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lumen_base.cms_setting: ~9 rows (approximately)
/*!40000 ALTER TABLE `cms_setting` DISABLE KEYS */;
INSERT IGNORE INTO `cms_setting` (`id`, `created_at`, `updated_at`, `group_setting`, `label`, `name`, `content`, `content_input_type`, `dataenum`, `helper`) VALUES
	(1, '2020-01-16 22:18:57', NULL, 'Email Setting', 'Email Sender', 'email_sender', 'info@crocodic.net', 'text', NULL, NULL),
	(2, '2020-01-16 22:22:16', NULL, 'Email Setting', 'Mail Driver', 'smtp_driver', 'smtp', 'select', 'smtp,mail,sendmail', NULL),
	(3, '2020-01-16 22:24:08', NULL, 'Email Setting', 'SMTP Host', 'smtp_host', 'smtp.mailgun.org', 'text', NULL, NULL),
	(4, '2020-01-16 22:24:54', NULL, 'Email Setting', 'SMTP Port', 'smtp_port', '587', 'text', NULL, 'default 25'),
	(5, '2020-01-16 22:24:56', NULL, 'Email Setting', 'SMTP Username', 'smtp_username', 'default@smtp.crocodic.net', 'text', NULL, NULL),
	(6, '2020-01-16 23:34:03', NULL, 'Email Setting', 'SMTP Password', 'smtp_password', 'crocodichebat', 'text', NULL, NULL),
	(7, '2020-01-17 08:09:32', NULL, 'Application Setting', 'Application Name', 'appname', 'Bukopin - Monitoring Aset', 'text', NULL, NULL),
	(8, '2020-01-17 08:12:26', NULL, 'Application Setting', 'Google API Key', 'google_api_key', 'AIzaSyBtO9883lZeEW1cMXTqrjtxYjeOwyFrymk', 'text', NULL, NULL),
	(9, '2020-01-17 08:13:48', NULL, 'Application Setting', 'Google FCM Key', 'google_fcm_key', NULL, 'text', NULL, NULL);
/*!40000 ALTER TABLE `cms_setting` ENABLE KEYS */;

-- Dumping structure for table lumen_base.cms_users
CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci,
  `roles_id` int(11) DEFAULT NULL,
  `md_cabang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lumen_base.cms_users: ~1 rows (approximately)
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT IGNORE INTO `cms_users` (`id`, `created_at`, `updated_at`, `name`, `email`, `image`, `password`, `roles_id`, `md_cabang_id`) VALUES
	(1, '2020-01-15 19:24:11', NULL, 'Admin Crocodic', 'admin@crocodic.com', NULL, '$2y$10$48ilfpW0c1ouWeEkOg.0tOzB.cA8mH0u9ACCgUgCNoUF5uWU.xY7G', 1, NULL);
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;

-- Dumping structure for table lumen_base.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lumen_base.migrations: ~5 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(60, '2020_01_06_115105_create_table_users', 1),
	(72, '2020_01_10_155246_create_table_privilage', 1),
	(73, '2020_01_11_054155_create_table_cms_users', 1),
	(74, '2020_01_15_021256_create_table_cms_setting', 2),
	(77, '2020_01_17_013124_create_table_cms_email_template', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table lumen_base.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table lumen_base.roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT IGNORE INTO `roles` (`id`, `created_at`, `updated_at`, `name`) VALUES
	(1, '2020-01-19 04:17:49', NULL, 'Super Administrator');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
