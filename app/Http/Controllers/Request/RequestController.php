<?php

namespace App\Http\Controllers\Request;

use Illuminate\Http\Request as Params;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use Illuminate\Support\Facades\Schema;

class RequestController extends Controller
{
    public function deleteValue(Params $params)
    {
        $table = $params->get("table");
        $column = $params->get("column");
        $id = $params->get("id");
        $url = $params->get("url");
        
        if (empty($table) && empty($column) && empty($id)) {
            return response()->json(["message"=>"Parameters is invalid"],400);
        }else{

            if (Schema::hasTable($table)) {
                if (Schema::hasColumn($table,$column)) {
                    
                    $data = DB::table($table)
                        ->where("id",$id)
                        ->first();

                    if ($data) {
                        API::removeFile($data->$column);

                        $image[$column] = "";
                        $update = DB::table($table)
                            ->where("id",$id)
                            ->update($image);
                        // dd($update);
                        if ($update) {
                            return redirect($params->get("url"));
                            // dview(urlGenerator());
                            // return back();
                            // return redirect()->back()->withinput();
                        }else{
                            return response()->json(["message"=>"failed update image is fails"],400);
                        }
                    }else{
                        return response()->json(["message"=>"delete image is fails"],400);
                    }
                }else{
                    return response()->json(["message"=>"column not found"],400);
                }
            }else{
                return response()->json(["message"=>"table not found"],400);
            }

        }
    }
}
