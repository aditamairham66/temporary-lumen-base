<!DOCTYPE html>
<?php
if($fileformat=="xls"){
    header("Pragma: public");
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . $filename . '.' . $fileformat . '"');
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>

    <table style="border-collapse: collapse;">
        <tr>
            <th align="center" colspan="11">REPORT DEPRESIASI ASET</th>
        </tr>
        <tr>
            <th align="center" colspan="11">Periode {{ $first_date }} - {{ $last_date }}</th>
        </tr>
        <tr>
            <th colspan="11"></th>
        </tr>
    </table>
    <table border="1" style="border-collapse: collapse;">
        <thead>
            <tr>
                <th rowspan="2" class="text-center align-middle">No Registrasi Aset</th>
                <th rowspan="2" class="text-center align-middle">No Registrasi Aset</th>
                <th rowspan="2" class="text-center align-middle">Nilai Perolehan</th>
                <th rowspan="2" class="text-center align-middle">Golongan</th>
                <th rowspan="2" class="text-center align-middle">Tanggal Perolehan</th>
                <th colspan="3" class="text-center">Depresiasi Straigh Line</th>
                <th colspan="3" class="text-center">Double Decline</th>
            </tr>
            <tr>
                <th class="text-center">Nilai Penyusutan</th>
                <th class="text-center">Akumulasi Penyusutan (Bulan Berjalan)</th>
                <th class="text-center">Nilai Buku</th>
                <th class="text-center">Nilai Penyusutan</th>
                <th class="text-center">Akumulasi Penyusutan (Bulan Berjalan)</th>
                <th class="text-center">Nilai Buku</th>
            </tr>
        </thead>
        <tbody>
            @foreach($table as $key => $val)
            <tr>
                <td>{{ $val['no_register'] }}</td>
                <td>{{ $val['name_asset'] }}</td>
                <td>{{ $val['harga_perolehan'] }}</td>
                <td>{{ $val['golongan'] }}</td>
                <td>{{ $val['tgl_perolehan'] }}</td>
                <td>{{ $val['depresiasi'] }}</td>
                <td>{{ $val['akumulasi_penyusutan'] }}</td>
                <td>{{ $val['nilai_buku'] }}</td>
                <td>{{ $val['depresiasidecline'] }}</td>
                <td>{{ $val['akumulasi_penyusutan'] }}</td>
                <td>{{ $val['nilai_buku'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>