<!DOCTYPE html>
<?php
if($fileformat=="xls"){
    header("Pragma: public");
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . $filename . '.' . $fileformat . '"');
}
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
    
    <table border="1" style="border-collapse: collapse;">
        @if($column)
            <tr>
                @if(in_array("link_nomor_register", $column))
                    <th>Barcode</th>
                @endif
                @if(in_array("nomor_register", $column))
                    <th>Nomor Register</th>
                @endif
                @if(in_array("nama_kategori", $column))
                    <th>Kategori</th>
                @endif
                @if(in_array("nama", $column))
                    <th>Nama Aset</th>
                @endif
                @if(in_array("nomor_po", $column))
                    <th>Nomor PO</th>
                @endif
                @if(in_array("kondisi", $column))
                    <th>Kondisi Aset</th>
                @endif
                @if(in_array("status_inventaris", $column))
                    <th>Status Inventaris</th>
                @endif
                @if(in_array("status_kepemilikan", $column))
                    <th>Status Kepemilikan</th>
                @endif
            </tr>
            @foreach($table as $row)
                <tr>
                    @foreach($column as $key => $array)
                        {{--@if($array=="link_nomor_register")
                        <td>
                            <img src="{{ $row->$array }}" style="height: auto !important;">
                        </td>
                        @else--}}
                        <td>{{ $row->$array }}</td>
                        {{--@endif--}}
                    @endforeach
                </tr>
            @endforeach
        @endif
    </table>

</body>
</html>