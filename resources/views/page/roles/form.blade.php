@extends("layout.index")
@section("content")
<div class="row">
    <div class="col-md-12 form-group">
        <a href="{{ adminpath($back) }}" class="btn btn-sm btn-outline-secondary">
            <i class="mdi mdi-reply text-primary"></i>Back to Home
        </a>
    </div>
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {!! showAlert() !!}
                <h4 class="card-title">{{ $page_name }}</h4>
                <form class="forms-sample" action="{{ adminpath($url) }}" method="POST">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{ (oldW('nama')?:$data->name) }}">
                        {!! hasError("nama") !!}
                    </div>
                    <a class="btn btn-light btn-sm" href="{{ adminpath($back) }}"s><i class="mdi mdi-reply text-primary"></i> Back</a>
                    @if(segmentPath(3)!="edit")
                    <button type="submit" name="submit" value="Save & Add More" class="btn btn-success btn-sm">Save & Add More</button>
                    @endif
                    <button type="submit" name="submit" value="Save" class="btn btn-success btn-sm">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.css') }}">
<style>
.btn{
    font-weight: 600;
}
</style>
@endpush
@push("js")
<script src="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
<script>
(function($) {
  'use strict';
    /*Summernote editor*/
    if ($("#summernoteExample").length) {
        $('#summernoteExample').summernote({
            height: 300,
            tabsize: 2
        });
    }
})(jQuery);
</script>
@endpush