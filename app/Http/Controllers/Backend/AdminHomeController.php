<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Helpers\Mailer;

class AdminHomeController extends Controller
{
    /**
     * Create a Admin Home Controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    // TODO HOME INDEX
    public function getIndex(Request $request)
    {
        return view("layout.home");
    }

}
