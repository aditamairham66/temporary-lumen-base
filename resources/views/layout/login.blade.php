
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{{ env('APP_NAME') }}</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('pearl-ui/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('pearl-ui/css/feather.css') }}">
  <link rel="stylesheet" href="{{ asset('pearl-ui/css/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('pearl-ui/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('pearl-ui/images/favicon.png') }}" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth login-full-bg">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-dark text-left p-5">
              <h2>Login</h2>
              <h4 class="font-weight-light">Halo! ayo kita mulai</h4>
              
              {!! showAlert() !!}

              <form class="pt-5" action="{{ url('admin/login') }}" method="post">
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                  <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username" value='{{ oldW("username") }}'>
                  <i class="mdi mdi-account"></i>
                  {!! hasError("username") !!}
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value='{{ oldW("password") }}'>
                  <i class="mdi mdi-eye"></i>
                  {!! hasError("password") !!}
                </div>
                <div class="mt-5">
                  <button class="btn btn-block btn-warning btn-lg font-weight-medium" type="submit">Login</button>
                </div>
                <div class="mt-3 text-center">

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('pearl-ui/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('pearl-ui/js/off-canvas.js') }}"></script>
    <script src="{{ asset('pearl-ui/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('pearl-ui/js/misc.js') }}"></script>
    <script src="{{ asset('pearl-ui/js/settings.js') }}"></script>
    <script src="{{ asset('pearl-ui/js/todolist.js') }}"></script>
  <!-- endinject -->
</body>
</html>