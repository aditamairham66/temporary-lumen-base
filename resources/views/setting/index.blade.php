@extends("layout.index")
@section("content")
<div class="row container">
    <div class="col-md-3 col-lg-3"></div>
    <div class="col-md-6 col-lg-7 grid-margin">
        <div class="card">
            <div class="card-body">
                @if(empty(g('group')))
                <div class="form-group">
                    <button class="btn btn-xs btn-primary pull-right"> <span class="fa fa-plus"></span> <b> Add Settings </b></button>        
                </div>
                <h6 class="card-title">Setting</h6>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Group</label>
                            <input type="text" class="form-control" name="group_setting" id="group_setting">
                        </div>
                        <div class="form-group">
                            <label>Label</label>
                            <input type="text" class="form-control" name="label" id="label">
                        </div>
                        <div class="form-group">
                            <label>Type</label>
                            <select name="content_input_type" id="content_input_type" class="form-control">
                                <option value="">Please select a Type**</option>
                                <option value="text">Text</option>
                                <option value="email">Email</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Radio / Select Data</label>
                            <input type="text" class="form-control" name="dataenum" id="dataenum" placeholder="Example : abc,def,ghi">
                        </div>
                        <div class="form-group">
                            <label>Label</label>
                            <input type="text" class="form-control" name="helper" id="helper">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" value="Save & Add More" class="btn btn-success btn-sm">Save & Add More</button>
                            <button type="submit" name="submit" value="Save" class="btn btn-success btn-sm">Save</button>
                        </div>
                    </div>
                </div>
                @else
                {!! showAlert() !!}

                <?php
                    $group = DB::table("cms_setting")
                        ->where("group_setting","LIKE","%".str_replace("-"," ",g('group'))."%")
                        ->get();
                ?>
                <h6 class="card-title">{{ str_replace("-"," ",g('group')) }}</h6>
                <form action="{{ adminpath('setting/show?group='.g('group')) }}" method="post">
                    @foreach($group as $key_form => $value_form)
                        @if($value_form->content_input_type=="text")
                        <div class="form-group">
                            <label>{{ $value_form->label }}</label>
                            <input type="text" name="{{ $value_form->name }}" id="{{ $value_form->name }}" class="form-control" value="{{ getSetting($value_form->name) }}">
                        </div>
                        @elseif($value_form->content_input_type=="select")
                        <?php
                            $dataenum = explode(',', $value_form->dataenum);
                            if ($dataenum) {
                                array_walk($dataenum, 'trim');
                            }
                        ?>
                        <div class="form-group">
                            <label>{{ $value_form->label }}</label>
                            <select name="{{ $value_form->name }}" id="{{ $value_form->name }}" class="form-control">
                                <option value="">** Please select {{ $value_form->label }}</option>
                                @foreach($dataenum as $key_dataenum => $value_dataenum)
                                    <option value="{{ $value_dataenum }}" @if($value_dataenum==getSetting($value_form->name)) selected @endif>{{ $value_dataenum }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    @endforeach
                    <div class='pull-right'>
                        <input type='submit' name='submit' value='Save' class='btn btn-success'/>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3 col-lg-3"></div>
</div>
@endsection

@push("css")
@endpush
@push("js")
@endpush