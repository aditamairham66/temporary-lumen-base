<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Model\ModelEmailTemplate;
use App\Helpers\MailSend;

class AdminEmailTemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getIndex()
    {
        $data['page_name'] = "Email Templates";
        $data['data'] = ModelEmailTemplate::get();
        return view("email-template.index", $data);
    }

    public function getAdd(Request $request)
    {
        $data['page_name'] = "Tambah Email Templates";
        $data['url'] = "email-template/insert";
        $data['data'] = new ModelEmailTemplate;
        $data['back'] = "email-template"; 
        return view("email-template.form",$data);
    }
    public function postAdd(Request $request)
    {
        $valid = [
            "name" => "required",
            "slug" => "required",
            "subject" => "required",
            "description" => "required",
            "from_email" => "email",
            "cc_email" => "email",
        ];
        $message = [
            "name.required" => "Nama Template tidak dapat dikosongkan",
            "slug.required" => "Slug tidak dapat dikosongkan",
            "subject.required" => "Subject tidak dapat dikosongkan",
            "description.required" => "Deskripsi tidak dapat dikosongkan",
        ];
        
        $valid = Validator::make($request->all(),$valid,$message);

        if ($valid->fails()) {
            return Web::withError("email-template/insert",$request->all(),$valid->errors());
        }else{
            $data['created_at'] = API::dateTime();
            $data['name'] = $request->name;
            $data['slug'] = str_replace(" ","-",$request->slug);
            $data['subject'] = $request->subject;
            $data['content'] = $request->content;
            $data['description'] = $request->description;
            $data['from_name'] = $request->from_name;
            $data['from_email'] = $request->from_email;
            $data['cc_email'] = $request->cc_email;

            $insert = ModelEmailTemplate::add($data);
            if ($insert) {
                return Web::msgPage("Berhasil memasukkan data Email Templates","success");
            }else{
                return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
            }
        }
    }
    public function getEdit(Request $request, $id)
    {
        $data['page_name'] = "Edit Email Templates";
        $data['url'] = "email-template/edit/".$id;
        $data['data'] = ModelEmailTemplate::search("id", $id);
        $data['back'] = "email-template";
        return view("email-template.form",$data);
    }
    public function postEdit(Request $request, $id)
    {
        $valid = [
            "name" => "required",
            "slug" => "required",
            "subject" => "required",
            "description" => "required",
            "from_email" => "email",
            "cc_email" => "email",
        ];
        $message = [
            "name.required" => "Nama Template tidak dapat dikosongkan",
            "slug.required" => "Slug tidak dapat dikosongkan",
            "subject.required" => "Subject tidak dapat dikosongkan",
            "description.required" => "Deskripsi tidak dapat dikosongkan",
        ];
        
        $valid = Validator::make($request->all(),$valid,$message);

        if ($valid->fails()) {
            return Web::withError("email-template/edit/".$id,$request->all(),$valid->errors());
        }else{
            $data['updated_at'] = API::dateTime();
            $data['name'] = $request->name;
            $data['slug'] = str_replace(" ","-",$request->slug);
            $data['subject'] = $request->subject;
            $data['content'] = $request->content;
            $data['description'] = $request->description;
            $data['from_name'] = $request->from_name;
            $data['from_email'] = $request->from_email;
            $data['cc_email'] = $request->cc_email;
    
            $insert = ModelEmailTemplate::edit($data,"id",$id);
            if ($insert) {
                return Web::msgPage("Berhasil memperbarui data Email Templates","success");
            }else{
                return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
            }
        }
    }
    public function getDelete(Request $request, $id)
    {
        $del = ModelEmailTemplate::remove("id",$id);
        if ($del) {
            return Web::msgPage("Berhasil menghapus data Email Templates ","success");
        }else{
            return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
        }
    }

}
