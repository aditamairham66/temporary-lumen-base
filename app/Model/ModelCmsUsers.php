<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;

class ModelCmsUsers extends Model
{
    protected static $table_name = "cms_users";
    private $name;
    private $email;
    private $image;
    private $password;
    private $roles_id;
    private $md_cabang_id;

    function setName($name) { 
        $this->name = $name; 
    }
    function getName() { 
        return $this->name; 
    }
    function setEmail($email) { 
        $this->email = $email; 
    }
    function getEmail() { 
        return $this->email; 
    }
    function setImage($image) { 
        $this->image = $image; 
    }
    function getImage() { 
        return $this->image; 
    }
    function setPassword($password) { 
        $this->password = $password; 
    }
    function getPassword() { 
        return $this->password; 
    }
    function setRoles_id($roles_id) { 
        $this->roles_id = $roles_id; 
    }
    function getRoles_id() { 
        return $this->roles_id; 
    }
    function setMd_cabang_id($md_cabang_id) { 
        $this->md_cabang_id = $md_cabang_id; 
    }
    function getMd_cabang_id() { 
        return $this->md_cabang_id; 
    }

    // TODO GET DATA
    public static function get()
    {
        $data = DB::table(static::$table_name)
            ->get();
        return $data;
    }

    // TODO GET DATA
    public static function findBy($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->first();
        return $save;
    } 

    // TODO SAVE DATA
    public static function add($data)
    {
        $save = DB::table(static::$table_name)
            ->insert($data);
        return $save;
    }

    // TODO UPDATE DATA
    public static function edit($data, $key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->update($data);
        return $save;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }

}
