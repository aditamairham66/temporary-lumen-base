<?php
if (!function_exists('urlImport')) {
    /**
     * @param $link, $table
     * @return string
     */
    function urlImport($link = null, $table = null)
    {
        if ($link != "" && $table != "") {
            return adminpath('import-data?l='.$link.'&table='.$table);
        }
    }
}

if (!function_exists('urlImportDone')) {
    /**
     * @param $link, $table, $file, $status
     * @return string
     */
    function urlImportDone($link = null, $table = null, $file = null, $status = null)
    {
        if ($link != "" && $table != "" && $file != null && $status != null) {
            return adminpath('import-data-done?l='.$link.'&table='.$table.'&file='.$file.'&import='.$status);
        }
    }
}