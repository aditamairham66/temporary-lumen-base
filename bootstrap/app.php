<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->withFacades(true, [
    'Illuminate\Support\Facades\Redirect' => 'Redirect'
]);
$app->bind('redirect', 'Laravel\Lumen\Http\Redirector');

// 1. FIRST you need to install session from "composer require illuminate/session:5.4.*" this important
// 2. copy file session.php from root laravel project -> config -> session.php and paste in lumen root lumen project -> config -> session.php
// 3. don't forgrt to make folder sessions in root storage -> framework 
// TODO REGISTER SESSION LUMEN
$app->singleton(Illuminate\Session\SessionManager::class, function () use ($app) {
    return $app->loadComponent('session', Illuminate\Session\SessionServiceProvider::class, 'session');
});
// TODO REGISTER SESSION LUMEN
$app->singleton('session.store', function () use ($app) {
    return $app->loadComponent('session', Illuminate\Session\SessionServiceProvider::class, 'session.store');
});

// TODO install this "composer require league/flysystem-aws-s3-v3"
// $app->singleton('filesystem', function ($app) {
//     return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
// });

// TODO install this "composer require league/flysystem-aws-s3-v3"
$app->configure('filesystems');
class_alias('Illuminate\Support\Facades\Storage', 'Storage');
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

// TODO REGISTER SESSION LUMEN
$app->middleware([
    \Illuminate\Session\Middleware\StartSession::class,
]);
// TODO REGISTER SESSION ERROS VARIABEL LIKE laravel [ $errors ]
$app->middleware([
    \Illuminate\View\Middleware\ShareErrorsFromSession::class,
]);

// TODO REGISTER MIDDLEWARE IN LUMEN
$app->routeMiddleware([
    'default' => App\Http\Middleware\MiddlewareAPIDefault::class,
    'api' => App\Http\Middleware\MiddlewareAPIRoute::class,
    'login' => App\Http\Middleware\MiddlewareWebLogin::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(App\Providers\AppServiceProvider::class); 
// 1. you must install package email from "composer require illuminate/mail:5.4.*"
// 2. you must copy mail.php from root laravel project -> config -> mail.php and paste in lumen root lumen project -> config -> mail.php
// TODO EMAIL CONFIGURE
$app->register(Illuminate\Mail\MailServiceProvider::class); 
// 1. you must install package maatwebsite from "composer require "maatwebsite/excel ~2.1.0"
// TODO MAATWEBSITE CONFIGURE
$app->register(Maatwebsite\Excel\ExcelServiceProvider::class);


/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../routes/web.php';
});


// TODO FILE CONFIG API
$app->configure("API");
// TODO FILE CONFIG WEB
$app->configure("Web");
// TODO REGISTER EMAIL LUMEN
$app->configure('services');
$app->configure('mail');
// $app->configure('mail');
// $app->configure('services');
$app->alias('mailer', Illuminate\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\Mailer::class);
$app->alias('mailer', Illuminate\Contracts\Mail\MailQueue::class);

return $app;
