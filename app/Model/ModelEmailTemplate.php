<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;

class ModelEmailTemplate extends Model
{
    private static $table_name = "cms_email_template";
    private $name;
    private $slug;
    private $subject;
    private $content;
    private $description;
    private $from_name;
    private $from_email;
    private $cc_email;

    function setName($name) { 
        $this->name = $name; 
    }
    function getName() { 
        return $this->name; 
    }
    function setSlug($slug) { 
        $this->slug = $slug; 
    }
    function getSlug() { 
        return $this->slug; 
    }
    function setSubject($subject) { 
        $this->subject = $subject; 
    }
    function getSubject() { 
        return $this->subject; 
    }
    function setContent($content) { 
        $this->content = $content; 
    }
    function getContent() { 
        return $this->content; 
    }
    function setDescription($description) { 
        $this->description = $description; 
    }
    function getDescription() { 
        return $this->description; 
    }
    function setFrom_name($from_name) { 
        $this->from_name = $from_name; 
    }
    function getFrom_name() { 
        return $this->from_name; 
    }
    function setFrom_email($from_email) { 
        $this->from_email = $from_email; 
    }
    function getFrom_email() { 
        return $this->from_email; 
    }
    function setCc_email($cc_email) { 
        $this->cc_email = $cc_email; 
    }
    function getCc_email() { 
        return $this->cc_email; 
    }

    // TODO GET DATA
    public static function get()
    {
        $data = DB::table(static::$table_name)
            ->get();
            
        return $data;
    }

    // TODO GET DATA
    public static function search($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->first();
        return $save;
    } 

    // TODO SAVE DATA
    public static function add($data)
    {
        $save = DB::table(static::$table_name)
            ->insert($data);
        return $save;
    }

    // TODO UPDATE DATA
    public static function edit($data, $key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->update($data);

        return $save;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
            
        return $save;
    }
}
