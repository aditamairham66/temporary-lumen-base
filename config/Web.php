<?php
return [
    "email_footer" => "Jangan membalas email ke alamat email ini. Email ini dikirim secara otomatis oleh sistem kami.",
    'MAIN_DB_DATABASE' => env('DB_DATABASE'),
];