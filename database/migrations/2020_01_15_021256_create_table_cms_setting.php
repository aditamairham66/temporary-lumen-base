<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCmsSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('cms_setting')) {
            Schema::create('cms_setting', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->timestamps();
                $table->string("group_setting",255)->nullable();
                $table->string("label",255)->nullable();
                $table->string("name",255)->nullable();
                $table->text("content")->nullable();
                $table->string("content_input_type",255)->nullable();
                $table->string("dataenum",255)->nullable();
                $table->string("helper",255)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('cms_setting')) {
            Schema::dropIfExists('cms_setting');        
        }
    }
}
