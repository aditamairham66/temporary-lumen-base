<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Model\ModelRoles;

class AdminRolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function getIndex()
    {
        $data['page_name'] = "Roles";
        $data['data'] = ModelRoles::get();
        return view("page.roles.index", $data);
    }

    public function getAdd(Request $request)
    {
        $data['page_name'] = "Tambah Roles";
        $data['url'] = "roles/add";
        $data['data'] = new ModelRoles;
        $data['back'] = "roles"; 
        return view("page.roles.form",$data);
    }
    public function postAdd(Request $request)
    {
        $valid = [
            "nama" => "required",
        ];
        $message = [
            "nama.required" => "Nama tidak boleh kosong",
        ];
        
        $valid = Validator::make($request->all(),$valid,$message);

        if ($valid->fails()) {
            return Web::withError("roles/add",$request->all(),$valid->errors());
        }else{
            $data['created_at'] = API::dateTime();
            $data['name'] = $request->nama;

            $insert = ModelRoles::add($data);
            if ($insert) {
                return Web::msgPage("Berhasil memasukkan data roles","success");
            }else{
                return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
            }
        }
    }
    public function getEdit(Request $request, $id)
    {
        $data['page_name'] = "Edit Roles";
        $data['url'] = "roles/edit/".$id;
        $data['data'] = ModelRoles::findBy("id", $id);
        $data['back'] = "roles";
        return view("page.roles.form",$data);
    }
    public function postEdit(Request $request, $id)
    {
        $data['updated_at'] = API::dateTime();
        $data['name'] = $request->nama;

        $insert = ModelRoles::edit($data,"id",$id);
        if ($insert) {
            return Web::msgPage("Berhasil memperbarui data roles","success");
        }else{
            return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
        }
    }
    public function getDelete(Request $request, $id)
    {
        $del = ModelRoles::remove("id",$id);
        if ($del) {
            return Web::msgPage("Berhasil menghapus data data roles ","success");
        }else{
            return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
        }
    }

}
