<!-- partial:../../partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
        <div class="nav-link d-flex">
            <div class="profile-image">
            <img src="{{ image() }}" alt="image"/>
            <span class="online-status online"></span> <!--change class online to offline or busy as needed-->
            </div>
            <div class="profile-name">
            <p class="name">
                {{ sessionGet("admin_name") }}
            </p>
            <p class="designation">
                {{ level() }}
            </p>
            </div>
        </div>
        </li>
        
        <li class="nav-item nav-category">
            <span class="nav-link">Menu</span>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ adminpath() }}">
                <i class="icon-layout menu-icon"></i>
                <span class="menu-title">Dashboard</span>
                {{--<span class="badge badge-primary badge-pill">1</span>--}}
            </a>
        </li>
        <!-- TODO SETTING -->
        @if(level()=="Super Administrator" || level()=="Admin Bukopin")
        <li class="nav-item nav-category">
            <span class="nav-link">Setting</span>
        </li>
        @endif
        @if(level()=="Super Administrator")
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#roles" aria-expanded="false" aria-controls="roles">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">Roles</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="roles">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ adminpath('roles') }}">List Roles</a></li>
                </ul>
            </div>
        </li>
        @endif
        @if(level()=="Super Administrator" || level()=="Admin Bukopin")
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manajemen-pengguna" aria-expanded="false" aria-controls="manajemen-pengguna">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">Manajemen Pengguna</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="manajemen-pengguna">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ adminpath('users') }}">List Pengguna</a></li>
                </ul>
            </div>
        </li>
        @endif
        @if(level()=="Super Administrator")
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#pengaturan" aria-expanded="false" aria-controls="pengaturan">
                <i class="mdi mdi-settings menu-icon"></i>
                <span class="menu-title">Pengaturan</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="pengaturan">
                <ul class="nav flex-column sub-menu">
                    <!-- <li class="nav-item"> <a class="nav-link" href="{{ adminpath('setting') }}">Setting</a></li> -->
                    <?php
                        $sidebar = DB::table("cms_setting")->select('group_setting')->groupby('group_setting')->get();
                        foreach ($sidebar as $key_sidebar => $value_sidebar) {
                    ?>
                    <li class="nav-item"><a class="nav-link" href="{{ adminpath('setting/show?group='.str_replace(" ","-",$value_sidebar->group_setting)) }}">{{ $value_sidebar->group_setting }}</a></li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#template-email" aria-expanded="false" aria-controls="template-email">
                <i class="icon-envelope-open menu-icon"></i>
                <span class="menu-title">Template Email</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="template-email">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ adminpath('email-template/insert') }}">Tambah Email Baru</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ adminpath('email-template') }}">Template Email</a></li>
                </ul>
            </div>
        </li>
        @endif
    </ul>
</nav>
<!-- partial -->