<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Web;
use App\Helpers\API;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class AdminImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    
    public function getIndex(Request $request)
    {
        if ($request->get('file') && $request->get('table')) {
            $table = $request->get('table');
            $file = base64_decode($request->get('file'));
            // $file = storage_path('app/'.$file);
            $file = public_path($file);
            $rows = Excel::load($file, function ($reader) {
            })->get();
            
            $countRows = ($rows)?count($rows):0;
            
            sessionPut('total_data_import', $countRows);

            $data_import_column = [];
            foreach ($rows as $value) {
                $a = [];
                foreach ($value as $k => $v) {
                    $a[] = $k;
                }
                if ($a && count($a)) {
                    $data_import_column = $a;
                }
                break;
            }

            $table_columns = DB::getSchemaBuilder()->getColumnListing($table);

            $data['table_columns'] = $table_columns;
            $data['data_import_column'] = $data_import_column;

            return view("import.import", $data);
        }else{
            return view("import.import");
        }
    }

    public function postUploadFile(Request $request)
    {
        $valid = Validator::make(
            [
                'file_upload'      => $request->file_upload,
                'extension' => strtolower($request->file_upload->getClientOriginalExtension()),
            ],
            [
                'file_upload'    => 'required',
                // 'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp',
                'extension'      => 'required|in:csv,xlsx,xls',
            ]
        );

        if ($valid->fails()) {
            return Web::withError("import-data?l=".$request->get('l').'&table='.$request->get('table'),null,$valid->errors());
        }else{
            $filepath = Web::UploadFile('file_upload','import-data');
            // $rows = Excel::load("storage/app/".$filepath, function ($reader) {
            // })->get();
            $base_64 = base64_encode($filepath);
            $url = adminpath('import-data?file='.$base_64.'&table='.$request->get('table').'&l='.$request->get('l').'&table='.$request->get('table').'');
            return redirect($url);
        }
    }

    public function postImportProses(Request $request)
    {
        $file_md5 = md5($request->get('file'));

        $select_column = sessionGet('select_column');
        $select_column = array_filter($select_column);
        $table_columns = DB::getSchemaBuilder()->getColumnListing($request->get('table'));

        $file = base64_decode($request->get('file'));
        $file = public_path($file);

        $rows = Excel::load($file, function ($reader) {
        })->get();

        $add = false;
        $data_import_column = [];
        foreach ($rows as $value) {
            $a = [];
            foreach ($select_column as $sk => $s) {
                $colname = $table_columns[$sk];

                //Skip if value is empty
                if ($value->$s == '') {
                    continue;
                    $add = false;
                }else if (empty($value->$s)) {
                    continue;
                    $add = false;
                }else{
                    $a[$colname] = $value->$s;
                    $add = true;
                }


            }

            try {

                if ($add) {
                    $a['created_at'] = date('Y-m-d H:i:s');
                    DB::table($request->get('table'))->insert($a);
                }
                $pro = Cache::increment('success_'.$file_md5);
            } catch (\Exception $e) {
                $e = (string) $e;
                Cache::put('error_'.$file_md5, $e, 500);
            }
        }

        return response()->json(['status' => true,"progress"=>100]);
    }

    public function postImportDone(Request $request)
    {
        sessionPut('select_column', $request->get('select_column'));
        return view("import.import");
    }
}
