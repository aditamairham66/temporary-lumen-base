<?php

use Illuminate\Database\Seeder;
use App\Helpers\API;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class CreateSeedCmsUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = DB::table("cms_users")
            ->where("email","admin@crocodic.com")
            ->first();

        if ($check) {
            $this->command->info('Updating the data CMS Users Invetaris failed, because data is same !');
        }else{
            $insert = DB::table("cms_users")
            ->insert([
                "created_at" => API::dateTime(),
                "roles_id" => 1,
                "name" => "Admin Crocodic",
                "password" => Hash::make("123456"),
                "email" => "admin@crocodic.com",
            ]);
    
            if ($insert) {
                $this->command->info('Updating the data CMS Users Invetaris completed !');
            }else{
                $this->command->info('Updating the data CMS Users Invetaris failed !');
            }
        }

        $check = DB::table("roles")
            ->where("name","Super Admin")
            ->first();

        if ($check) {
            $this->command->info('Updating the data Roles Invetaris failed, because data is same !');
        }else{
            $insert = DB::table("roles")
            ->insert([
                "created_at" => API::dateTime(),
                "name" => "Super Admin",
            ]);
    
            if ($insert) {
                $this->command->info('Updating the data Roles Invetaris completed !');
            }else{
                $this->command->info('Updating the data Roles Invetaris failed !');
            }
        }
    }
}
