@extends("layout.index")
@section("content")
<div class="row">
    @if(in_array(level(), ['Super Administrator','Admin Bukopin']))
    <div class="col-md-12 form-group">
        <a href="{{ adminpath($back) }}" class="btn btn-sm btn-outline-secondary">
            <i class="mdi mdi-reply text-primary"></i>Back to Home
        </a>
    </div>
    @endif
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {!! showAlert() !!}
                <h4 class="card-title">{{ $page_name }}</h4>
                <form class="forms-sample" enctype="multipart/form-data" action="{{ adminpath($url) }}" method="POST">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" placeholder="Nama" value="{{ (oldW('nama')?:$data->name) }}">
                        {!! hasError("nama") !!}
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-envelope-open"></i>
                                </span>
                            </div>
                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ (oldW('email')?:$data->email) }}">
                        </div>
                        {!! hasError("email") !!}
                    </div>
                    <div class="form-group">
                        <label>Photo</label>
                        <input type="file" name="image" class="form-control" placeholder="Nama">
                        {!! hasError("image") !!}
                    </div>
                    @if(in_array(level(), ['Super Administrator','Admin Bukopin']))
                    <div class="form-group">
                        <label>Roles</label>
                        <select name="role" id="role" class="js-example-basic-single forms-sample" style="width:100%">
                            <option value="">Select Roles **</option>
                            @foreach($roles as $key => $value)
                            <option value="{{ $value->id }}" @if($data->roles_id==$value->id) selected @elseif(oldW('role')==$value->id) selected @endif>{{ $value->name }}</option>
                            @endforeach
                        </select>
                        {!! hasError("role") !!}
                    </div>
                    @endif
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        {!! hasError("password") !!}
                    </div>
                    @if(segmentPath('3')=="edit")
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                        {!! hasError("confirm_password") !!}
                    </div>
                    @endif
                    @if(in_array(level(), ['Super Administrator','Admin Bukopin']))
                    <a class="btn btn-light btn-sm" href="{{ adminpath($back) }}"s><i class="mdi mdi-reply text-primary"></i> Back</a>
                    @endif
                    @if(segmentPath(3)!="edit")
                    <button type="submit" name="submit" value="Save & Add More" class="btn btn-success btn-sm">Save & Add More</button>
                    @endif
                    <button type="submit" name="submit" value="Save" class="btn btn-success btn-sm">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.css') }}">
<style>
.btn{
    font-weight: 600;
}
</style>
@endpush
@push("js")
<script src="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
<script>
(function($) {
  'use strict';
    /*Summernote editor*/
    if ($("#summernoteExample").length) {
        $('#summernoteExample').summernote({
            height: 300,
            tabsize: 2
        });
    }
})(jQuery);
</script>
@endpush