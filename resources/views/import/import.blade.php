@extends("layout.index")
@section("content")
<div class="row">
    <div class="col-md-12 col-lg-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">Import Data</h4>

                @if(g('file')=="")
                <div class="wrapper mb-5 mt-4">
                    <span class="badge badge-warning text-white">Note : </span>
                    <p class="d-inline ml-3 text-muted">File type supported only : XLS, XLSX, CSV .</p>
                </div>

                <form action="{{ urlImport(g('l'),g('table')) }}" method="post" class="forms-sample" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>File XLS / CSV</label>
                        <input type="file" name="file_upload" id="file_upload" class="form-control" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                        <!-- <input type="file" name="file_upload" id="file_upload" class="form-control" > -->
                        {!! hasError("file_upload") !!}
                    </div>

                    <div class="form-group pull-right">
                        <a onclick="if(confirm('Are you sure want to leave ?')) location.href='{{ adminpath(g('l')) }}'" href='javascript:;' class="btn btn-sm btn-light">Cancel</a>
                        <button type="submit" class="btn btn-sm btn-success">Upload</button>
                    </div>
                </form>
                @elseif(g('file') && g('import')=="")
                <p class="card-description"></p>
                <form class="forms-sample" action="{{ urlImportDone(g('l'),g('table'),g('file'),1) }}" method="post" class="forms-sample" enctype="multipart/form-data">
                    
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr style='text-transform: capitalize'>
                                    @foreach($table_columns as $k=>$column)
                                        <?php
                                        $help = '';
                                        if ($column == 'id' || $column == 'created_at' || $column == 'updated_at' || $column == 'deleted_at') continue;
                                        if (substr($column, 0, 3) == 'id_') {
                                            $relational_table = substr($column, 3);
                                            $help = "<a href='#' title='This is foreign key, so the System will be inserting new data to table `$relational_table` if doesn`t exists'><strong>(?)</strong></a>";
                                        }
                                        ?>
                                        <th data-no-column='{{$k}}'>{{ $column }} {!! $help !!}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach($table_columns as $k=>$column)
                                        <?php if ($column == 'id' || $column == 'created_at' || $column == 'updated_at' || $column == 'deleted_at') continue;?>
                                        <td data-no-column='{{$k}}'>
                                            <select class='form-control select_column' name='select_column[{{$k}}]'>
                                                <option value=''>** Set Column for {{$column}}</option>
                                                @foreach($data_import_column as $import_column)
                                                    <option value='{{$import_column}}'>{{$import_column}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="wrapper mb-5 mt-4">
                        <span class="badge badge-warning text-white">Note : </span>
                        <p class="d-inline ml-3 text-muted">You can select column from dropdown .</p>
                    </div>

                    <div class="form-group pull-right">
                        <a onclick="if(confirm('Are you sure want to leave ?')) location.href='{{ adminpath(g('l')) }}'" href='javascript:;' class="btn btn-sm btn-light">Cancel</a>
                        <input name='submit' onclick='return check_selected_column()' value='Import Data' type="submit" class="btn btn-sm btn-success" />
                    </div>
                </form>
                @push('js')
                    <script type="text/javascript">
                        $(function () {
                            var total_selected_column = 0;
                            setInterval(function () {
                                total_selected_column = 0;
                                $('.select_column').each(function () {
                                    var n = $(this).val();
                                    if (n) total_selected_column = total_selected_column + 1;
                                })
                            }, 200);
                        })

                        function check_selected_column() {
                            var total_selected_column = 0;
                            $('.select_column').each(function () {
                                var n = $(this).val();
                                if (n) total_selected_column = total_selected_column + 1;
                            })
                            if (total_selected_column == 0) {
                                swal("Oops...", "Please at least 1 column that should adjusted...", "error");
                                return false;
                            } else {
                                return true;
                            }
                        }
                    </script>
                @endpush
                @else

                    <div class="wrapper mb-5 mt-4">
                        <span class="badge badge-warning text-white">Note : </span>
                        <p class="d-inline ml-3 text-muted" style='font-weight: bold' id='status-import'>Please wait import data is running .</p>
                    </div>
                    
                    <div class="template-demo">
                        <div class="progress progress-lg">
                        <div id='progress-import' class="progress-bar bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        @push('js')
                            <script type="text/javascript">
                                $(function () {
                                    var total = {{ intval(sessionGet('total_data_import')) }};

                                    var int_prog = setInterval(function () {

                                        $.post("{{ adminpath('do-import-chunk?file='.g('file')) }}", {resume: 1, table: "{{ g('table') }}"} , function (resp) {
                                            console.log(resp.progress);
                                            $('#progress-import').css('width', resp.progress + '%');
                                            $('#status-import').html("<i class='fa fa-spin fa-spinner'></i> Please wait importing... (" + resp.progress + "%)");
                                            $('#progress-import').attr('aria-valuenow', resp.progress);
                                            if (resp.progress >= 100) {
                                                $('#status-import').addClass('text-success').html("<i class='fa fa-check-square-o'></i> Import Data Completed !");
                                                clearInterval(int_prog);
                                            }
                                        })


                                    }, 2500);

                                    $.post("{{ adminpath('do-import-chunk').'?file='.g('file') }}", {table: "{{ g('table') }}"}, function (resp) {
                                        if (resp.status == true) {
                                            $('#progress-import').css('width', '100%');
                                            $('#progress-import').attr('aria-valuenow', 100);
                                            $('#status-import').addClass('text-success').html("<i class='fa fa-check-square-o'></i> Import Data Completed !");
                                            clearInterval(int_prog);
                                            $('#upload-footer').show();
                                            console.log('Import Success');
                                        }
                                    });

                                })
                            </script>
                        @endpush
                    </div>

                    <br>
                    <div class="form-group">
                        <div class="pull-right">
                            <a href="{{ adminpath(g('l')) }}" class='btn btn-success btn-sm'>Finish</a>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
@endpush