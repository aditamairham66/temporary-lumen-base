<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Illuminate\Http\Request;
use App\Helpers\API;

// $app->get('/', function () use ($app) {
//     return $app->version();
// });

/**
 * You can change path on here
 * 
 *  @return string
 */ 
$base_api = "api";
$base_admin = "admin";

$app->get('/', function () use ($app) {
    return abort("404");
});
$app->get('/admin', function () use ($app) {
    return redirect("/admin/login");
});

// Backend DOCUMENTATION

$app->get("/$base_admin/login",'Backend\AdminLoginController@getLogin');
$app->post("/$base_admin/login",'Backend\AdminLoginController@postLogin');
$app->get("/$base_admin/logout",'Backend\AdminLoginController@getLogout');

// Backend Website
$app->group(['middleware' => 'login'], function () use ($app,$base_admin) {
    $app->get("/$base_admin",'Backend\AdminHomeController@getIndex');
    /*
    *
    * Start menu route admin for website role.
    *
    */ 
    // TODO Users
    $app->get("/$base_admin/users",'Backend\AdminUsersController@getIndex');
    $app->get("/$base_admin/users/add",'Backend\AdminUsersController@getAdd');
    $app->post("/$base_admin/users/add",'Backend\AdminUsersController@postAdd');
    $app->get("/$base_admin/users/edit/{id}",'Backend\AdminUsersController@getEdit');
    $app->post("/$base_admin/users/edit/{id}",'Backend\AdminUsersController@postEdit');
    $app->get("/$base_admin/users/delete/{id}",'Backend\AdminUsersController@getDelete');
    // SETTING
    $app->get("/$base_admin/setting",'Backend\AdminSettingController@getIndex');
    $app->get("/$base_admin/setting/show",'Backend\AdminSettingController@getIndex');
    $app->post("/$base_admin/setting/show",'Backend\AdminSettingController@postEditSettingGroup');
    // CMS EMAIL TEMPLATE
    $app->get("/$base_admin/email-template",'Backend\AdminEmailTemplateController@getIndex');
    $app->get("/$base_admin/email-template/insert",'Backend\AdminEmailTemplateController@getAdd');
    $app->post("/$base_admin/email-template/insert",'Backend\AdminEmailTemplateController@postAdd');
    $app->get("/$base_admin/email-template/edit/{id}",'Backend\AdminEmailTemplateController@getEdit');
    $app->post("/$base_admin/email-template/edit/{id}",'Backend\AdminEmailTemplateController@postEdit');
    $app->get("/$base_admin/email-template/delete/{id}",'Backend\AdminEmailTemplateController@getDelete');
    // IMPORT
    $app->get("/$base_admin/import-data",'Backend\AdminImportController@getIndex');
    $app->post("/$base_admin/import-data",'Backend\AdminImportController@postUploadFile');
    $app->post("/$base_admin/import-data-done",'Backend\AdminImportController@postImportDone');
    $app->post("/$base_admin/do-import-chunk",'Backend\AdminImportController@postImportProses');
    // TODO Roles
    $app->get("/$base_admin/roles",'Backend\AdminRolesController@getIndex');
    $app->get("/$base_admin/roles/add",'Backend\AdminRolesController@getAdd');
    $app->post("/$base_admin/roles/add",'Backend\AdminRolesController@postAdd');
    $app->get("/$base_admin/roles/edit/{id}",'Backend\AdminRolesController@getEdit');
    $app->post("/$base_admin/roles/edit/{id}",'Backend\AdminRolesController@postEdit');
    $app->get("/$base_admin/roles/delete/{id}",'Backend\AdminRolesController@getDelete');
});

// Backend DOCUMENTATION

// ======================================================================================================================= //

// -- API DOCUMENTATION -- //

// GENERATE API TOKEN
$app->get('/'.$base_api.'/request-token', 'API\GenerateToken@getGenerate');
// DELETE API TOKEN
$app->get('/'.$base_api.'/delete-token', 'API\GenerateToken@getDeleteToken');

// API ROUTE WITHOUT USERS ID
$app->group(['middleware' => 'default'], function () use ($app,$base_api) {

});

// API ROUTE YOU CAN GET USERS ID
$app->group(['middleware' => 'api'], function () use ($app,$base_api) {
    // EXAMPLE
    $app->get("/".$base_api."/example", 'API\GenerateToken@getxample');
    
});

// -- API DOCUMENTATION -- //



