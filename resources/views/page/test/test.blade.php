@extends("layout.index")
@section("content")
<div class="row">
    <div class="col-md-12 col-lg-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h6 class="card-title">Name Page</h6>
                <div class="map-container">
                <div id="map-with-marker" class="google-map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
@endpush
@push("js")
@endpush