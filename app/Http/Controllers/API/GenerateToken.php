<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;

class GenerateToken extends Controller
{
    public function getGenerate(Request $request)
    {
        $useragent = API::isBrowserRequest($request->header("User-Agent"));
        if ($useragent) {
            return abort("404");
        } else {
            $token = $request->header("Authorization");
            if ($token) {
                $decrypt_token = base64_decode(str_replace("Basic ","",$token));
                if ($decrypt_token=="adminbukopin:123456") {
                    $token = API::generateNewToken();

                    $config['base_path'] = url();
                    $data['token'] = $token;
                    $data['configuration'] = $config;
            
                    $response['message'] = "success";
                    $response['data'] = $data;
                    return response()->json($response, 200);
                }else{
                    $response['message'] = "Invalid token";
                    return response()->json($response,400);
                }
            }else{
                $response['message'] = "Invalid token";
                return response()->json($response,400);
            }
        }
    }

    public function getDeleteToken(Request $request)
    {
        API::forgetToken($request->header("Authorization"));
        $response['message'] = "Access Token deleted";
        return response()->json($response, 200);
    }
}
