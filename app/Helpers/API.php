<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class API
{
    // TODO API KEY VALIDATION MD5
    public static function checkAPIKey($encryption, $time)
    {
        $key = config("API.key_api");
        return (md5($key . $time) == $encryption ? TRUE : FALSE);
    }

    // TODO generate new token
    public static function generateNewToken()
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $generate_new_token = substr(str_shuffle(str_repeat($pool, 5)), 0, 16);

        $data = [
            "ip_address"=> $_SERVER["REMOTE_ADDR"],
            "user_agent"=> $_SERVER['HTTP_USER_AGENT']
        ];

        Cache::forever("token_".$generate_new_token, $data);
        // Cache::put("token_".$generate_new_token, $data, Carbon::now()->addDay(30));
        return $generate_new_token;
    }

    // TODO update token api
    public static function generateUpdateToken($id_user, $token)
    {
        $replace = str_replace("Bearer ","",$token);

        $data = [
            "ip_address"=> $_SERVER["REMOTE_ADDR"],
            "user_agent"=> $_SERVER['HTTP_USER_AGENT'],
            "id_users" => $id_user,
        ];

        Cache::forever("token_".$replace, $data);
        // Cache::put("token_".$replace, $data, Carbon::now()->addDay(30));
    }

    // TODO get token api
    public static function getTokenApi($token)
    {
        $replace = str_replace("Bearer ","",$token);
        $data = Cache::get("token_".$replace);
        return $data;
    }

    // TODO forget token
    public static function forgetToken($token) {
        $replace = str_replace("Bearer ","",$token);
        Cache::forget("token_".$replace);
    }
    
    // TODO USER AGENT VALIDATION
    public static function isBrowserRequest($userAgent)
    {
        $browsers = config("API.user_agent");
        $isBrowser = false;
        foreach($browsers as $browser){
            if(strpos($userAgent, $browser) !==  false){
            $isBrowser = true;
            break;
            }
        }
        return $isBrowser;
    }

    // TODO VALIDATOR API
    public static function Validator($data = [])
    {
        $validator = Validator::make(Request::all(), $data);
     
        if ($validator->fails()) {
            $result = array();
            $message = $validator->errors();
            // $result['api_status'] = 0;
            // $result['api_title'] = 'Request is not valid';
            // $result['api_message'] = $message->all(':message')[0];
            $result['message'] = $message->all(':message')[0];
            $res = response()->json($result, 400);
            $res->send();
            exit;
        }
    }

    // TODO LOGGER ERRORS API
    public static function Log($filename, $content)
    {
        $all = Request::all();
        foreach ($all as $key => $value) {
            if (strlen($value) > 255) continue;
            $data[$key] = $value;
        }
        $data['ip'] = Request::ip();//$_SERVER['REMOTE_ADDR'];
        // $data['code'] = API::ServerCode();

        $log = new Logger($filename);
        $log->pushHandler(new StreamHandler(base_path() . '/storage/logs/' . $filename . '.log', Logger::INFO));
        $log->info($content, $data);
    }

    // TODO ERRORS API
    public static function failed($error)
    {
        $response['api_status'] = 0;
        $response['api_title'] = 'Something went wrong';
        $response['api_message'] = 'Please try again';
        return $response;
    }

    // TODO VIEW URL FILE
    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } elseif (file_exists($check_public)) {
            return url($path);
        } elseif (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    // TODO DELETE FILE FILE
    public static function removeFile($path)
    {
        if ($path != '') {
            $file_public = public_path($path);
            $file_storage = storage_path('app/' . $path);

            if (file_exists($file_public)) {
                if (!unlink($file_public)) {
                    return false;
                } else {
                    return true;
                }
            } elseif (file_exists($file_storage)) {
                if (!unlink($file_storage)) {
                    return false;
                } else {
                    return true;
                }

            } else {
                return true;
            }
        } else {
            return true;
        }
    }
    
    // TODO FORMAT TIME NOW
    public static function dateTime()
    {
        date_default_timezone_set('Asia/Jakarta');
        return date("Y-m-d H:i:s");
    }

    // TODO UPLOAD FILE
    public static function uploadFile($file, $extension = 'jpg', $directory_path = 'upload', $id = '')
    {
        $path = public_path('uploads/' . $directory_path);
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $decode = base64_decode($file);
        $filename = ($id?$id."_":"").strtotime(date('Y-m-d H:i:s')) . "." . $extension;
        $image_path = 'uploads/' . $directory_path . '/' . $filename;
        $upload_path = public_path($image_path);

        if (file_put_contents($upload_path, $decode)) {
            $url = url($image_path);
            try {
                if (!file($url)) {
                    return null;
                } else {
                    return $image_path;
                }
            } catch (\Exception $e) {
                API::removeFile($image_path);
                return null;
            }
        } else {
            return null;
        }
    }

    public static function getLevel($users_id)
    {
        $users = DB::table("users")
            ->where("id",$users_id)
            ->first()->roles_id;

        $role_id = ($users?:null);
        $role  = DB::table("roles")
            ->where("id",$role_id)
            ->first()->name;

        return ($role?:false);
    }

    public static function getUsers($users_id)
    {
        $users = DB::table("users")
            ->where("id",$users_id)
            ->first();
        return ($users?:false);
    }

}
