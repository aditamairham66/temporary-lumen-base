@extends("layout.index")
@section("content")
<div class="row form-group">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-0">{{ $page_name }}</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-flex">
                            <div class="form-group align-items-center">
                                <a href="{{ adminpath('email-template/insert') }}" class="btn btn-xs btn-primary"><span class="icon-plus"></span> <b>Add data</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <div class="bg-dark px-4 py-2 rounded d-flex">
                            <i class="icon-envelope-open text-white icon-lg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        {!! showAlert() !!}
        <div class="row">
            <div class="col-md-12">
                <table id="order-listing" class="table">
                    <thead>
                        <tr>
                            <th>Nama Template</th>
                            <th>Slug</th>
                            <th class="text-right">action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->slug }}</td>
                            <td class="text-right">
                                <div class="btn-group">
                                    <a href="{{ adminpath('email-template/edit/'.$value->id) }}" class="btn btn-success icon-btn"><span class="icon-eye menu-icon"></span></a>
                                    <a onclick="showSwal('backend-delete','{{ adminpath("email-template/delete/".$value->id) }}')" href="javascript:void(0)" class="btn btn-danger icon-btn"><span class="icon-trash menu-icon"></span></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")

@endpush
@push("js")
<!-- Custom js for this page-->
<script src="{{ asset('pearl-ui/js/data-table.js') }}"></script>
  <!-- End custom js for this page-->
@endpush