<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;

class ModelRoles extends Model
{
    private static $table_name = "roles";
    private $nama;

    // TODO Nama
    function setNama($nama) { 
        $this->nama = $nama; 
    }
    function getNama() { 
        return $this->nama; 
    }

    // TODO GET DATA
    public static function get()
    {
        $data = DB::table(static::$table_name)
            ->orderby("name","asc")
            ->get();

        return $data;
    }

    // TODO GET DATA
    public static function findBy($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->first();

        return $save;
    } 

    // TODO SAVE DATA
    public static function add($data)
    {
        $save = DB::table(static::$table_name)
            ->insert($data);
        return $save;
    }

    // TODO UPDATE DATA
    public static function edit($data, $key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->update($data);

        return $save;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
            
        return $save;
    }
}
