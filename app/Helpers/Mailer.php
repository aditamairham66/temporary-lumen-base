<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Helpers\API;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
// TODO use mail
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class Mailer
{
    private $template;
    private $to;
    private $subject;
    private $cc;
    private $sender;
    private $content;

    // TODO SETTING PARAMS
    function setTemplate($template) { 
        $this->template = $template; 
    }
    function setTo($to) { 
        $this->to = $to; 
    }
    function setSubject($subject) { 
        $this->subject = $subject; 
    }
    function setCC($cc) { 
        $this->cc = $cc; 
    }
    function setSender($email = []) { 
        $this->sender = ['email'=>$email['email'],'name'=>$email['name']]; 
    }
    function setContent($content = []) { 
        $this->content = $content; 
    }

    public static function Send($template=null , $to=null , $subject=null , $cc=null , $sender=null , $content=null )
    {
        $mail = new Mailer();
        $mail->setTemplate($template);
        $mail->setTo($to);
        $mail->setSubject($subject);
        $mail->setCC($cc);
        $mail->setSender($sender);
        $mail->setContent($content);
        $mail->SendTemplate();
        file_put_contents("mail.log", $mail);
        if ($mail) {
            return true;
        }else{
            return false;
        }
    }

    function SendTemplate()
    {
        // Config::set('mail.driver',config('SMTPEmail.smtp_driver'));
        // Config::set('mail.host',config('SMTPEmail.smtp_host'));
        // Config::set('mail.port',config('SMTPEmail.smtp_port'));
        // Config::set('mail.username',config('SMTPEmail.smtp_username'));
        // Config::set('mail.password',config('SMTPEmail.smtp_password'));
        // Config::set('mail.encryption','tls');

        $to = $this->to;
        $data = $this->content;
        $template = $this->template;

        $template = self::first('cms_email_template', ['slug' => $template]);
        $html = $template->content;

        foreach ($data as $key => $val) {
            $html = str_replace('['.$key.']', $val, $html);
            $template->subject = str_replace('['.$key.']', $val, $template->subject);
        }
        $subject = ($this->subject ? $this->subject : $template->subject);

        try {
            $log = Mail::send("emails.blank", ['content' => $html], function ($message) use ($to, $subject, $template) {
                $message->priority(1);
                $message->to($to);
    
                if ($template->from_email) {
                    $from_name = ($template->from_name) ?: config('SMTPEmail.app_name');
                    $message->from($template->from_email, $from_name);
                }
    
                if ($template->cc_email) {
                    $message->cc($template->cc_email);
                }
    
                $message->subject($subject);
            });
            file_put_contents("mail.log", $log);
            return true;
        } catch (\Throwable $th) {
            file_put_contents("mail.log", $th);
            return $th;
        }
    }
    public static function parseSqlTable($table)
    {

        $f = explode('.', $table);

        if (count($f) == 1) {
            return ["table" => $f[0], "database" => env('DB_DATABASE')];
        } elseif (count($f) == 2) {
            return ["database" => $f[0], "table" => $f[1]];
        } elseif (count($f) == 3) {
            return ["table" => $f[0], "schema" => $f[1], "table" => $f[2]];
        }

        return false;
    }
    public static function pk($table)
    {
        return self::findPrimaryKey($table);
    }
    public static function findPrimaryKey($table)
	{
		if(!$table)
		{
			return 'id';
		}
		
		$pk = DB::getDoctrineSchemaManager()->listTableDetails($table)->getPrimaryKey();
		if(!$pk) {
            return null;
		}
		return $pk->getColumns()[0];	
	}
    public static function first($table, $id)
    {
        $table = self::parseSqlTable($table)['table'];
        if (is_array($id)) {
            $first = DB::table($table);
            foreach ($id as $k => $v) {
                $first->where($k, $v);
            }

            return $first->first();
        } else {
            $pk = self::pk($table);

            return DB::table($table)->where($pk, $id)->first();
        }
    }
}
