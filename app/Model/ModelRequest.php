<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use App\Traits\File;
use App\Traits\Status;

class ModelRequest extends Model
{
    use File, Status;

    private static $table_name = "request_survey";
    private static $table_name_survey = "pengajuan_survey";

    // TODO GET DATA
    public static function get()
    {
        $data = DB::table(static::$table_name)
            ->get();

        return $data;
    }

    // TODO GET DATA
    public static function findBy($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->first();
        return $save;
    } 

    // TODO SAVE DATA
    public static function add($data)
    {
        $save = DB::table(static::$table_name)
            ->insert($data);
        return $save;
    }

    // TODO SAVE DATA
    public static function addSurvey($data)
    {
        $save = DB::table(static::$table_name_survey)
            ->insert($data);
        return $save;
    }

    // TODO FIND multiple
    public static function findMulti($data = [])
    {
        $save = DB::table(static::$table_name)
            ->where($data)
            ->get();
        return $save;
    }

    // TODO UPDATE DATA
    public static function edit($data, $key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->update($data);

        return $save;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
            
        return $save;
    }

    public static function allData()
    {
        $level = level();
        $id_cabang = sessionGet("admin_cabang");

        $hapus_buku = DB::table("hapus_buku")
            ->pluck("inventaris_id")
            ->toArray();

        $approve_asset = DB::table("approval_inventaris")
            ->select("approval_inventaris.inventaris_id")
            ->join("cms_users","cms_users.id","=","approval_inventaris.created_by")
            ->where(["cms_users.roles_id"=>5,"status"=>"Approved"])
            ->pluck("approval_inventaris.inventaris_id")
            ->toArray();

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->where(function ($q) use ($level, $id_cabang)
            {
                if (!in_array($level, ["Super Administrator","Admin Bukopin","Admin Manage Service"])) {
                    $q->where("inventaris.md_cabang_id", $id_cabang);
                }
            })
            ->groupby("inventaris.id")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            if (in_array($value->id, $approve_asset)) {
                if (in_array($value->id, $hapus_buku)) {
                    continue;
                }else{
                    $value->btn = self::switchButton($value->kondisi);
                    $value->btntwo = self::switchButtonTwo($value->status_inventaris);
                    $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
                    $value->statusPangajuan = self::pengajuanAsset($value->id);
                    $value->link_nomor_register = self::generateQRcode($value->nomor_register);
    
                    $list[] = $value;
                }
            }
        }

        return $list;
    }

    public static function allDataSubmission()
    {
        $level = level();
        if ($level=="Admin Manage Service") {
            $hapus_buku = DB::table(static::$table_name_survey)
                ->pluck("inventaris_id")
                ->toArray();
        }

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->groupby("inventaris.id")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            if ($level=="Admin Manage Service") {
                if (in_array($value->id, $hapus_buku)) {
                    $value->btn = self::switchButton($value->kondisi);
                    $value->btntwo = self::switchButtonTwo($value->status_inventaris);
                    $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
                    $value->statusPangajuan = false;
                    $value->btnStatusRequest = false;
                    $value->link_nomor_register = self::generateQRcode($value->nomor_register);
    
                    $list[] = $value;
                }
            }
        }

        return $list;
    }

    public static function allDataChecklist()
    {
        $cabang = sessionGet("admin_cabang");
        $level = level();

        $approve_asset = DB::table("approval_inventaris")
                ->select("approval_inventaris.inventaris_id")
                ->join("cms_users","cms_users.id","=","approval_inventaris.created_by")
                ->where(["cms_users.roles_id"=>5,"status"=>"Approved"])
                ->pluck("approval_inventaris.inventaris_id")
                ->toArray();

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi",
                "request_survey.id as id_request_survey",
                "request_survey.status as status_request"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->join("request_survey","request_survey.inventaris_id","=","inventaris.id")
            ->whereNotNull("request_survey.status")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            $value->btn = self::switchButton($value->kondisi);
            $value->btntwo = self::switchButtonTwo($value->status_inventaris);
            $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
            $value->statusPangajuan = self::pengajuanAsset($value->id);
            $value->btnStatusRequest = self::status($value->status_request);
            $value->link_nomor_register = self::generateQRcode($value->nomor_register);

            $list[] = $value;
        }

        return $list;
    }

    private static function pengajuanAsset($id)
    {
        $data = DB::table(static::$table_name_survey)
            ->where("inventaris_id",$id)
            ->where("status","Waiting")
            ->first();

        return ($data?true:false);
    }

    public static function listSurveyor()
    {
        $data = DB::table("users")
            ->select("id","nik","name")
            ->where("roles_id",10)
            ->get();

        foreach ($data as $key => $value) {
            $value->id = ($value->id?:0);
            $value->nik = ($value->nik?:"");
            $value->name = ($value->name?:"");
        }

        return $data;
    }

    public static function SearchRequest($id)
    {
        $survey = DB::table("request_survey")
            ->select(
                "users.name as name_users",
                "request_survey.komentar_survey as comment",
                "request_survey.foto_sku",
                "request_survey.foto_fisik_1",
                "request_survey.foto_fisik_2",
                "request_survey.foto_fisik_3",
                "md_divisi.nama as nama_divisi",
                "request_survey.address",
                "md_lantai.name as lantai",
                "md_ruangan.name as ruang",
                "kondisi_asset.kondisi",
                "request_survey.latitude",
                "request_survey.longitude",
                "request_survey.inventaris_id",
                "request_survey.status"
            )
            ->join("users","users.id","=","request_survey.surveyor_id")
            ->join("md_divisi","md_divisi.id","=","request_survey.md_divisi_id")
            ->join("md_lokasi","md_lokasi.id","=","request_survey.md_lokasi_id")
            ->join("md_lantai","md_lantai.id","=","request_survey.md_lantai_id")
            ->join("md_ruangan","md_ruangan.id","=","request_survey.md_ruangan_id")
            ->join("kondisi_asset","kondisi_asset.id","=","request_survey.kondisi_asset_id")
            ->where("request_survey.id",$id)
            ->first();

        return $survey;
    }

    public static function searchInventaris($id)
    {
        $inventaris = DB::table("inventaris")
            ->select(
                "inventaris.file",
                "inventaris.foto_fisik_1",
                "inventaris.foto_fisik_2",
                "inventaris.foto_fisik_3",
                "md_divisi.nama as nama_divisi",
                "md_lokasi.alamat as nama_address",
                "md_lantai.name as lantai",
                "md_ruangan.name as ruang",
                "kondisi_asset.kondisi",
                "md_lokasi.latitude",
                "md_lokasi.longitude"
            )
            ->join("md_divisi","md_divisi.id","=","inventaris.md_divisi_id")
            ->join("md_lokasi","md_lokasi.id","=","inventaris.md_lokasi_id")
            ->join("md_lantai","md_lantai.id","=","inventaris.md_lantai_id")
            ->join("md_ruangan","md_ruangan.id","=","inventaris.md_ruangan_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->where("inventaris.id",$id)
            ->first();

        return $inventaris;
    }

    public function editPengajuan($data ,$value)
    {
        $table = DB::table(static::$table_name_survey)
            ->where($value)
            ->update($data);
        return $data;
    }

    public static function allHistorySurvey()
    {
        $level = level();
        $id_cabang = sessionGet("admin_cabang");

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi",
                "request_survey.id as id_request_survey",
                "request_survey.status as status_request"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->join("request_survey","request_survey.inventaris_id","=","inventaris.id")
            ->where(function ($q) use ($id_cabang, $level)
            {
                if ($level!="Super Administrator" && $level!="Admin Bukopin" && $level!="Admin Manage Service") {
                    $q->where("inventaris.md_cabang_id", $id_cabang);
                }
            })
            ->whereNotNull("request_survey.status")
            ->where("request_survey.status","<>","Waiting")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            $value->btn = self::switchButton($value->kondisi);
            $value->btntwo = self::switchButtonTwo($value->status_inventaris);
            $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
            $value->statusPangajuan = self::pengajuanAsset($value->id);
            $value->btnStatusRequest = self::status($value->status_request);
            $value->link_nomor_register = self::generateQRcode($value->nomor_register);

            $list[] = $value;
        }

        return $list;
    }

    public static function reportAsset()
    {
        $level = level();
        $id_cabang = sessionGet("admin_cabang");

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->where(function ($q) use ($id_cabang, $level)
            {
                if (!in_array($level,["Super Administrator","Admin Bukopin","Admin Manage Service"])) {
                    $q->where("inventaris.md_cabang_id", $id_cabang);
                }
            })
            ->where("inventaris.status_inventaris","Aktif")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            $value->btn = self::switchButton($value->kondisi);
            $value->btntwo = self::switchButtonTwo($value->status_inventaris);
            $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
            $value->statusPangajuan = self::pengajuanAsset($value->id);
            $value->link_nomor_register = self::generateQRcode($value->nomor_register);

            $list[] = $value;
        }

        return $list;
    }

    public static function exportAsset()
    {
        $level = level();
        $id_cabang = sessionGet("admin_cabang");

        $data = DB::table("inventaris")
            ->select(
                "inventaris.*",
                "md_kategori.nama_kategori",
                "kondisi_asset.kondisi"
            )
            ->join("md_kategori","md_kategori.id","=","inventaris.md_kategori_id")
            ->join("kondisi_asset","kondisi_asset.id","=","inventaris.kondisi_asset_id")
            ->where(function ($q) use ($id_cabang, $level)
            {
                if (!in_array($level,["Super Administrator","Admin Bukopin","Admin Manage Service"])) {
                    $q->where("inventaris.md_cabang_id", $id_cabang);
                }
            })
            ->where("inventaris.status_inventaris","Aktif")
            ->get();

        $list = [];
        foreach ($data as $key => $value) {
            $value->btn = self::switchButton($value->kondisi);
            $value->btntwo = self::switchButtonTwo($value->status_inventaris);
            $value->btnthree = self::switchButtonThree($value->status_kepemilikan);
            $value->statusPangajuan = self::pengajuanAsset($value->id);
            $value->link_nomor_register = self::generateQRcode($value->nomor_register);

            $list[] = $value;
        }

        return $list;
    }
}
