<?php
if (!function_exists('urlGenerator')) {
    /**
     * @return \Laravel\Lumen\Routing\UrlGenerator
     */
    function urlGenerator() {
        return new \Laravel\Lumen\Routing\UrlGenerator(app());
    }
}

// TODO FUNCTION ASSET
if (!function_exists('asset')) {
    /**
     * @param $path
     * @param bool $secured
     *
     * @return string
     */
    function asset($path, $secured = false) {
        return urlGenerator()->asset($path, $secured);
    }
}

// TODO BACKEND URL
if (!function_exists('adminpath')) {
    /**
     * @param $path
     * @param bool $secured
     *
     * @return string
     */
    function adminpath($url = null) {
        return url("/admin/".$url);
    }
}

// TODO STORAGE PATH
if (! function_exists('storage_path')) {
    /**
     * Get the path to the storage folder.
     *
     * @param  string  $path
     * @return string
     */
    function storage_path($path = '')
    {
        // return $this->basePath().'/storage'.($path ? '/'.$path : $path);
        return app()->storagePath($path);
    }
}

// TODO PUBLIC PATH
if (! function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = null)
    {
        // return rtrim(app()->basePath('public/' . $path), '\\/\\');
        return env('PUBLIC_PATH', base_path('public')) . ($path ? '/' . $path : $path);
    }
}

// TODO SETTING
if (!function_exists('getSetting')) {
    /**
     * 
     *
     * @param  string  $key
     * @return string
     */
    function getSetting($key = null)
    {
        return App\Helpers\Web::getSetting($key);
    }
}

if (!function_exists('deleteImage')) {
    /**
     * 
     *
     * @param  string  $table, $column, $id
     * @return string
     */
    function deleteImage($table = null, $column = null, $id = null)
    {
        return adminpath("delete/".$table."/".$column."/".$id);
    }
}

// TODO DD CUSTOM
if (!function_exists('dview')) {
    /**
     * 
     *
     * @param  string  $params
     * @return View
     */
    function dview($params = null)
    {
        echo '<pre>';
        echo '<small>';
        print_r($params);
        echo '</small>';
        echo '</pre>';
        die();
    }
}

// TODO LINK DELETE FILE BACKEND
if (!function_exists('RemoveFile')) {
    /**
     * .
     *
     * @param  string  $table, $column, $id, $url
     * @return string
     */
    function RemoveFile($table, $column, $id, $url)
    {
        return adminpath('delete?table='.$table."&column=".$column."&id=".$id."&url=".$url);
    }
}

// TODO RETURN FULL URL PAGE
if (!function_exists('fullUrl')) {
    /**
     * .
     *
     * @return string
     */
    function fullUrl()
    {
        return app('url')->full();
    }
}

// TODO RANGE TWO DATE
if (!function_exists('dateRange')) {
    /**
     * .
     *
     * @param  string  $dateone, $datetwo
     * @return string
     */
    function dateRange($dateone, $datetwo)
    {
        $datetime1 = strtotime($dateone);
        $datetime2 = strtotime($datetwo);

        $secs = $datetime2 - $datetime1;
        $days = $secs / 86400;

        return $days;
    }
}

// TODO FUNCTION GENERATE BULAN
if (!function_exists("generateMonth")) {
    /**
     *
     *
     * @param  string  $month
     * @return string
     */
    function generateMonth($month = null)
    {
        if (empty($month)) {
            return false;
        }else{
            $bulan = 12;
            $generate = $bulan * $month;
            return $generate;
        }
    }
}

// TODO FUNCTION ADD DAYS
if (!function_exists('addDays')) {
    /**
     *
     *
     * @param  string  $date,$day
     * @return string
     */
    function addDays($date, $day)
    {
        return date('Y-m-d', strtotime("+".$day." days", strtotime($date)));
    }
}

// TODO FUNCTION ADD MONTH
if (!function_exists('addMonths')) {
    /**
     *
     *
     * @param  string  $date,$month
     * @return string
     */
    function addMonths($date, $month)
    {
        return date('Y-m-d', strtotime("+".$month." months", strtotime($date)));
    }
}

// TODO FUNCTION GENERATE TIMES
if (!function_exists('timeAgo')) {
    /**
     *
     *
     * @param  string  $datetime
     * @param  boolean  $full
     * @return string
     */
    function timeAgo($datetime, $full = false)
    {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);
        
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
        
        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }
        
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' lalu' : 'baru saja';
    }
}

// TODO REDUCTION DATE
if (!function_exists('Reductiondate')) {
    /**
     *
     *
     * @param  string  $dateone, $datetwo
     * @return string
     */
    function Reductiondate($dateone, $datetwo)
    {
        $datetime1 = strtotime($dateone);
        $datetime2 = strtotime($datetwo);

        return $secs = $datetime2 - $datetime1;
    }
}

// DATE RANGA
if (!function_exists('dateRemain')) {
    /**
     *
     *
     * @param  string  $date1, $date2
     * @return string
     */
    function dateRemain($date1, $date2)
    {
        if (new \Datetime($date1) > new \Datetime($date2)) {
            return false;
        }else{
            $diff = abs(strtotime($date2) - strtotime($date1));

            $years = floor($diff / (365*60*60*24)); 
    
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
    
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
    
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
    
            $minuts = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
    
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
    
            return ($years ? $years.' tahun ' : '').($months ? $months.' bulan ' : '').($days ? $days.' hari ' : '');
        }
    }
}

// FUNCTION SKIP INVENTRAIS
if (!function_exists('skipInventaris')) {
    /**
     *
     *
     * @param  string  $data
     * @return string
     */
    function skipInventaris($data)
    {
        $roles = level();
        if ($roles=="Checker (Web)") {
            $role_id = 4;
        }elseif ($roles=="Approver (Web)") {
            $role_id = 5;
        }else{
            $role_id = 0;
        }

        $check = App\Model\ModelStatusInventaris::findMultiple([
            'inventaris_id'=>$data,
            'role'=>$role_id,
            "status"=>"Approved"
        ]);

        return $check;
    }
}

if (!function_exists("ExistsParams")) {
    /**
     *
     *
     * @param  string  $params
     * @return string
     */
    function ExistsParams($params)
    {
        $error = app("request")->session()->get("errors");
        foreach ($params as $key => $value) {
            $check = App\Helpers\Web::have($error, $value);
            if ($check) {
                return true;
            }else{
                return false;
            }
        }
    }
}