<!-- partial:../../partials/_navbar.html -->
<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success">
<!-- <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-dark"> -->
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="{{ adminpath() }}"><img src="{{ asset('pearl-ui/images/favicon.png') }}" alt="logo"/></a>
    <a class="navbar-brand brand-logo-mini" href="{{ adminpath() }}"><img src="{{ asset('pearl-ui/images/favicon.png') }}" alt="logo"/></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
        <span class="mdi mdi-menu"></span>
    </button>
    <ul class="navbar-nav">
        <li class="nav-item d-none d-lg-block">
        <a class="nav-link">
            <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
        </a>
        </li>
    </ul>
    <ul class="navbar-nav navbar-nav-right">
        
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <img src="{{ image() }}" alt="image" style="display: initial !important;background: #FFF !important;object-fit: cover;">
            <span class="d-none d-lg-inline">{{ sessionGet("admin_name") }}</span>
        </a>
        <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
            <a class="dropdown-item" href="{{ adminpath('users/edit/'.sessionGet('admin_id')) }}">
                <i class="mdi mdi-cached mr-2 text-success"></i>
                Profile
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="showSwal('backend-logout','{{ adminpath("logout")}}')" href="javascript::void(0)">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Signout
            </a>
        </div>
        </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
    <span class="mdi mdi-menu"></span>
    </button>
    </div>
</nav>
<!-- partial -->