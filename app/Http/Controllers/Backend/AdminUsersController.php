<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Support\Facades\Hash;
use App\Model\ModelCmsUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;

class AdminUsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->roles = DB::table("roles")
            ->get();
    }

    public function getIndex()
    {
        $data['page_name'] = "Users";
        $data['data'] = ModelCmsUsers::get();
        return view("page.users.index", $data);
    }

    public function getAdd(Request $request)
    {
        $data['page_name'] = "Tambah Users";
        $data['url'] = "users/add";
        $data['data'] = new ModelCmsUsers;
        $data['back'] = "users";
        $data['roles'] = $this->roles;
        return view("page.users.form",$data);
    }
    public function postAdd(Request $request)
    {
        $valid = [
            "nama" => "required",
            "email" => "required|email",
            "image" => "image",
            "role" => "required",
            "password" => "required",
        ];
        $message = [
            "nama.required" => "Nama tidak boleh kosong",
            "email.required" => "Email tidak boleh kosong",
            "email.email" => "Email harus berformat email yang benar",
            "role.required" => "Role tidak boleh kosong",
            "password.required" => "Password tidak boleh kosong",
        ];
        
        $valid = Validator::make($request->all(),$valid,$message);

        if ($valid->fails()) {
            return Web::withError("users/add",$request->all(['nama','email',"password",'role','cabang']),$valid->errors());
        }else{
            $data['created_at'] = API::dateTime();
            $data['name'] = $request->nama;
            $data['email'] = $request->email;
            $data['image'] = Web::UploadFile("image","cms_users");
            $data['password'] = Hash::make($request->password);
            $data['roles_id'] = $request->role;
            $data['md_cabang_id'] = $request->cabang;

            $insert = ModelCmsUsers::add($data);
            if ($insert) {
                return Web::msgPage("Berhasil memasukkan data Users","success");
            }else{
                return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
            }
        }
    }
    public function getEdit(Request $request, $id)
    {
        $data['page_name'] = "Edit Users";
        $data['url'] = "users/edit/".$id;
        $data['data'] = ModelCmsUsers::findBy("id",$id);
        $data['back'] = "users";
        $data['roles'] = $this->roles;
        return view("page.users.form",$data);
    }
    public function postEdit(Request $request, $id)
    {
        $valid = [
            "confirm_password" => "required_with:password|same:password",
        ];
        
        $valid = Validator::make($request->all(),$valid);

        if ($valid->fails()) {
            return Web::withError("users/edit/".$id,$request->all(),$valid->errors());
        }else{
            $data['updated_at'] = API::dateTime();
            $data['name'] = $request->nama;
            $data['email'] = $request->email;
            if ($request->hasFile("image")) {
                $data['image'] = Web::UploadFile("image","cms_users");
            }
            if ($request->password) {
                $data['password'] = Hash::make($request->password);
                $data['forgot_password'] = $request->password;
            }
            if (in_array(level(), ['Super Administrator','Admin Bukopin'])) {
                $data['roles_id'] = $request->role;
            }

            $insert = ModelCmsUsers::edit($data,"id",$id);
            
            if ($insert) {
                return Web::msgPage("Berhasil memperbarui data Users","success");
            }else{
                return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
            }
        }
    }
    public function getDelete(Request $request, $id)
    {
        $del = ModelCmsUsers::remove("id",$id);
        if ($del) {
            return Web::msgPage("Berhasil menghapus data Users","success");
        }else{
            return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
        }
    }

}
