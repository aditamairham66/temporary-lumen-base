<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Auth
{
    // GET IMAGE 
    public static function Image()
    {
        $data = DB::table("cms_users")
            ->where("id",app("request")->session()->get("admin_id"))
            ->first();

        $image = ($data->image=="" ? API::file(app("request")->session()->get("admin_image")) : API::file($data->image)) ;
        $ret = ($image) ? $image : API::file("pearl-ui/images/favicon.png");

        return $ret;
    }

    // GET LEVEL 
    public static function Level()
    {
        $data = DB::table("roles")
            ->where("id",app("request")->session()->get("admin_roles"))
            ->first();

        $ret = ($data->name=="" ? "" : $data->name );
        return $ret;
    }
}
