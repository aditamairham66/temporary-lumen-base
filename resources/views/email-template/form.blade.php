@extends("layout.index")
@section("content")
<div class="row">
    <div class="col-md-12 form-group">
        <a href="{{ adminpath($back) }}" class="btn btn-sm btn-outline-secondary">
            <i class="mdi mdi-reply text-primary"></i>Back to Home
        </a>
    </div>
    
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {!! showAlert() !!}
                <h4 class="card-title">{{ $page_name }}</h4>
                <form class="forms-sample" action="{{ adminpath($url) }}" method="POST">
                    <div class="form-group">
                        <label>Nama Template</label>
                        <input type="text" name="name" class="form-control" placeholder="Nama Template" value="{{ $data->name }}">
                        {!! hasError("name") !!}
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                        <input type="text" name="slug" class="form-control" placeholder="Slug" value="{{ $data->slug }}">
                        {!! hasError("slug") !!}
                    </div>
                    <div class="form-group">
                        <label>Subject </label>
                        <input type="text" name="subject" class="form-control" placeholder="Subject" value="{{ $data->subject }}">
                        {!! hasError("subject") !!}
                    </div>
                    <div class="form-group">
                        <label>Konten</label>
                        <textarea name="content" id="summernoteExample" cols="30" rows="10">{!! $data->content !!}</textarea>
                        {!! hasError("content") !!}
                    </div>
                    <div class="form-group">
                        <label>Deskripsi </label>
                        <input type="text" name="description" class="form-control" placeholder="Deskripsi" value="{{ $data->description }}">
                        {!! hasError("description") !!}
                    </div>
                    <div class="form-group">
                        <label>Nama Pengirim </label>
                        <input type="text" name="from_name" class="form-control" placeholder="Nama Pengirim " value="{{ $data->from_name }}">
                        {!! hasError("from_name") !!}
                    </div>
                    <div class="form-group">
                        <label>Dari Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-envelope-open"></i>
                                </span>
                            </div>
                            <input type="email" name="from_email" class="form-control" placeholder="Dari Email" value="{{ $data->from_email }}">
                        </div>
                        {!! hasError("from_email") !!}
                    </div>
                    <div class="form-group">
                        <label>CC Email</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-envelope-open"></i>
                                </span>
                            </div>
                            <input type="email" name="cc_email" class="form-control" placeholder="CC Email" value="{{ $data->cc_email }}">
                        </div>
                        {!! hasError("cc_email") !!}
                    </div>
                    <a class="btn btn-light btn-sm" href="{{ adminpath($back) }}"s><i class="mdi mdi-reply text-primary"></i> Back</a>
                    @if(segmentPath(3)!="edit")
                    <button type="submit" name="submit" value="Save & Add More" class="btn btn-success btn-sm">Save & Add More</button>
                    @endif
                    <button type="submit" name="submit" value="Save" class="btn btn-success btn-sm">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push("css")
<link rel="stylesheet" href="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.css') }}">
<style>
.btn{
    font-weight: 600;
}
</style>
@endpush
@push("js")
<script src="{{ asset('pearl-ui/vendors/summernote/dist/summernote-bs4.min.js') }}"></script>
<script>
(function($) {
  'use strict';
    /*Summernote editor*/
    if ($("#summernoteExample").length) {
        $('#summernoteExample').summernote({
            height: 300,
            tabsize: 2
        });
    }
})(jQuery);
</script>
@endpush