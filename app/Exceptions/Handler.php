<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
/**
 * METHOD NOT ALLOWED
 */ 
// TODO
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
/**
 * PAGE NOT FOUND
 */ 
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Helpers\API;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof MethodNotAllowedHttpException) {
            $useragent = API::isBrowserRequest($request->header("User-Agent"));
            if ($useragent) {
                return response(view("errors.405"), 405);
            }else{
                $response['message'] = 'Method not Allowed';
                return response()->json($response, 405);
            }
        }else if($e instanceof NotFoundHttpException){
            $useragent = API::isBrowserRequest($request->header("User-Agent"));
            if ($useragent) {
                return response(view("errors.404"), 404);
            }else{
                $response['message'] = 'Page Not Found';
                return response()->json($response, 404);
            }
        }
        
        return parent::render($request, $e);
    }
}
