<?php

if (!function_exists('level')) {
    /**
     * LEVEL WEB BACKEND
     * 
     * @return string
     */
    function level()
    {
        return App\Helpers\Auth::Level();
    }
}

if (!function_exists("image")) {
    /**
     * IMAGE WEB BACKEND
     * 
     * @return string
     */
    function image()
    {
        return App\Helpers\Auth::Image();
    }
}