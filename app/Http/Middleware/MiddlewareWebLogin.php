<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class MiddlewareWebLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public $attributes;

    // TODO MIDDLEWARE API
    public function handle($request, Closure $next)
    {
        if (!$request->session()->get("admin_id")) {
            app('request')->session()->flash("msg_type","bg-warning");
            app('request')->session()->flash("msg_text","Silakan masukkan username dan password Anda");
            return redirect("admin/login");
        }

        return $next($request);
    }
}
