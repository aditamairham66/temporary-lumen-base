<div class="modal fade" id="googlemaps-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Browse Map</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input id="input-search-autocomplete" class="controls pac-input" autofocus type="text" placeholder="Search location here...">
                <div id="type-selector" class="controls hide type-selector">
                    <input type="radio" name="type" id="changetype-all" checked="checked">
                    <label for="changetype-all">All</label>

                    <input type="radio" name="type" id="changetype-establishment">
                    <label for="changetype-establishment">Establishments</label>

                    <input type="radio" name="type" id="changetype-address">
                    <label for="changetype-address">Addresses</label>

                    <input type="radio" name="type" id="changetype-geocode">
                    <label for="changetype-geocode">Geocodes</label>
                </div>
                <div class="map" id='map'></div>
                <br/>
                <p>
                    <span class="text-info" style="font-weight: bold">Current Location :</span><br/>
                    <span id='current-location-span'>{{ ($data->koordinat ? $data->koordinat : "") }}</span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="setIt()" data-dismiss="modal">Set It</button>
            </div>
        </div>
    </div>
</div>

@push("css")
<style>
    .map {
        height: 400px;
    }

    .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }

    .pac-container {
        z-index: 9999999 !important;
    }

    .pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
    }

    .pac-input:focus {
        border-color: #4d90fe;
    }

    .pac-container {
        font-family: Roboto;
    }

    .type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
    }

    .type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
</style>
@endpush

@push("js")
<script src="https://maps.googleapis.com/maps/api/js?key={{ (getSetting('google_api_key') ? getSetting('google_api_key') : '') }}&libraries=places"
            async defer></script>
<script>
var address_temp, latitude_temp, longitude_temp;

function setIt() {
    console.log(address_temp);
    $('#google-map-address').val(address_temp);
    $("#input-latitude").val(latitude_temp);
    $("#input-longitude").val(longitude_temp);
}

var is_init_map = false;

function showMapModal() {
    var api_key = "{{ (getSetting('google_api_key') ? getSetting('google_api_key') : '') }}";

    if (api_key == '') {
        alert('GOOGLE_API_KEY is missing, please set at setting !');
        return false;
    }

    $('#googlemaps-modal').modal('show');
}

$('#googlemaps-modal').on('shown.bs.modal', function () {
    if (is_init_map== false) {
        console.log('Init Map');
        initMap();
        is_init_map= true;
    }
});

var geocoder;

function initMap() {
    geocoder = new google.maps.Geocoder();
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 0, lng: 0 },
        zoom: 12
    });

    var marker_default_location = new google.maps.Marker({
        map: map,
        draggable: true,
        icon : "{{ asset('pearl-ui/images/google/map.png') }}"
    });

    var input = /** @type  {!HTMLInputElement} */(
        document.getElementById('input-search-autocomplete'));

    var types = document.getElementById('type-selector');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    var infoWindow = new google.maps.InfoWindow();

    // Try HTML5 geolocation.

    @if(!$data->latitude && !$data->longitude)

    latitude_temp = 0;
    longitude_temp = 0;
    var pos = {
        lat: latitude_temp,
        lng: longitude_temp
    };
    map.setCenter(pos);
    map.setZoom(1);

    @else
    var pos = {
            lat: Number(<?php echo ($data->latitude ? $data->latitude : 0); ?>),
            lng: Number(<?php echo ($data->longitude ? $data->longitude : 0); ?>)
        };

    latitude_temp = Number(<?php echo ($data->latitude ? $data->latitude : 0); ?>);
    longitude_temp = Number(<?php echo ($data->longitude ? $data->longitude : 0); ?>);

    address_temp = "<?php echo ($data->koordinat ? $data->koordinat : ""); ?>";

    map.setCenter(pos);
    map.setZoom(19);

    marker_default_location.setPosition(pos);

    infoWindow.close();
    infoWindow.setContent("<?php echo ($data->koordinat ? $data->koordinat : ""); ?>");
    infoWindow.open(map, marker_default_location);

    @endif

    google.maps.event.addListener(marker_default_location, 'dragend', function (marker_default_location) {

        geocoder.geocode({
            latLng: marker_default_location.latLng
        }, function (responses) {
            if (responses && responses.length > 0) {
                address = responses[0].formatted_address;
            } else {
                address = 'Cannot determine address at this location.';
            }

            address_temp = address;

            infoWindow.setContent(address);

            $('#current-location-span').text(address);

        });

        var latLng = marker_default_location.latLng;
        latitude = latLng.lat();
        longitude = latLng.lng();

        latitude_temp = latitude;
        longitude_temp = longitude;

    });

    autocomplete.addListener('place_changed', function () {
        infoWindow.close();
        marker_default_location.setVisible(false);

        var place = autocomplete.getPlace();

        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker_default_location.setPosition(place.geometry.location);
        marker_default_location.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();

        address_temp = address;

        $('#current-location-span').text(address);

        infoWindow.setContent(address);

        latitude_temp = latitude;
        longitude_temp = longitude;

        infoWindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infoWindow.open(map, marker_default_location);
    });

    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function () {
            autocomplete.setTypes(types);
        });
    }

    setupClickListener('changetype-all', []);
    setupClickListener('changetype-address', ['address']);
    setupClickListener('changetype-establishment', ['establishment']);
    setupClickListener('changetype-geocode', ['geocode']);
}
</script>
@endpush