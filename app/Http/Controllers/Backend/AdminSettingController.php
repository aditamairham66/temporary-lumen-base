<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Web;

class AdminSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getIndex(Request $request)
    {
        return view("setting.index");
    }

    public function postEditSettingGroup(Request $request)
    {
        $group =  DB::table("cms_setting")
            ->where("group_setting","LIKE","%".str_replace("-"," ",g('group'))."%")
            ->get();

        $next = false;

        foreach ($group as $key => $value) {
            if (Web::checkSetting($value->name)) {
                $up = Web::getSettingUpdate($value->name, $request->input($value->name));
                if ($up) {
                    $next = true;
                }
            }else{
                $in = Web::getSettingInsert($value->name, $request->input($value->name));
                if ($in) {
                    $next = true;
                }
            }
        }
        
        if ($next) {
            return Web::msgPage("Data setting berhasil diperbarui","success");
        }else{
            return Web::msgPage("Oops ada yang salah, silakan coba lagi","error");
        }
    }
}
