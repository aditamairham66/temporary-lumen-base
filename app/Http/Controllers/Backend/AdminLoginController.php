<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\API;
use App\Helpers\Web;
use Illuminate\Support\Facades\Hash;
use App\Model\ModelCmsUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class AdminLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function getLogin()
    {
        return view("layout.login");
    }
    public function postLogin(Request $request)
    {
        $rules = [
            "username" => "required|email",
            "password" => "required",
        ];

        $message = [
            "username.required" => "Username tidak boleh kosong",
            "username.email" => "Username harus berformat email yang benar",
            "password.required" => "Password tidak boleh kosong",
        ];
        
        $valid = Validator::make($request->all(),$rules,$message);
        if ($valid->fails()) {
            return Web::withError("login",$request->all(),$valid->errors());
        }else{
            // TODO Check username
            $check = DB::table("cms_users")
            ->where("email","LIKE","%".$request->username."%")
            ->first();

            if ($check) {
                if (!Hash::check($request->password, $check->password)) {
                    return Web::msgPage("Opps, Nama pengguna atau kata sandi salah","bg-warning");           
                }else{
                    $request->session()->put("admin_email", $check->email);
                    $request->session()->put("admin_name", $check->name);
                    $request->session()->put("admin_id", $check->id);
                    $request->session()->put("admin_image", $check->image);
                    $request->session()->put("admin_roles", $check->roles_id);

                    return redirect("admin");
                }
            }else{
                return Web::msgPage("Opps, Nama pengguna atau kata sandi salah","bg-warning");
            }
        }
    }
    public function getLogout(Request $request)
    {
        $request->session()->forget(['admin_email','admin_name','admin_id','admin_image']);
        return redirect("admin/login");
    }
}
